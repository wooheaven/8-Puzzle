import json

# read Ants
with open('Ant.json') as fr:
    Ant = json.load(fr)

# add Ants
target_count = 30
ant_count = len(Ant)
while ant_count <= target_count:
    Ant['a'+str(ant_count)] = Ant['a'+str(ant_count-1)]
    ant_count += 1

# write Ants
with open('Ant.json', 'w', encoding='utf8') as fw:
    json.dump(Ant, fw, indent=4, ensure_ascii=False)
