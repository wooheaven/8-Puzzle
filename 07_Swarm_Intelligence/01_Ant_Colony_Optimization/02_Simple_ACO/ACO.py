from collections import defaultdict
import json
import numpy as np


class Environment:
    def __init__(self, n, e):
        self.node = n
        self.edge = e

    def get_edges(self, current_node, earlier_node):
        edges_list = self.node.get(current_node)
        edges_dict = defaultdict(lambda: defaultdict(str))
        for tmp_edge in edges_list:
            tmp_nodes = self.edge[tmp_edge]['node']
            if earlier_node in tmp_nodes:
                continue
            edges_dict[tmp_edge] = self.edge[tmp_edge]
        return edges_dict

    def move(self, a, ratio):
        n = a.get('current_node')
        e = a.get('edge')
        next_node = ''
        my_nodes = self.edge.get(e).get('node')
        for tmp_node in my_nodes:
            if tmp_node != n:
                next_node = tmp_node
                break
        tmp_pheromone = self.edge.get(e).get('pheromone')
        tmp_pheromone *= (1 + ratio)
        self.edge[e]['pheromone'] = tmp_pheromone
        a['earlier_node'] = a['current_node']
        a['current_node'] = next_node
        a['edge'] = ''
        if next_node == 'n2' and a['nest_to_food'] is False:
            a['nest_to_food'] = True
        if next_node == 'n0' and a['nest_to_food']:
            a['food_to_nest'] = True

    def pheromones_evaporate(self, ratio):
        for tmpe_e in self.edge.keys():
            evaporation = (1 - ratio) * env.edge.get(tmpe_e)['pheromone']
            env.edge[tmpe_e]['pheromone'] = evaporation


# read Node
with open('Node.json') as f:
    node = json.load(f)
# read Edge
with open('Edge.json') as f:
    edge = json.load(f)
# read Ants
with open('Ant.json') as f:
    Ant = json.load(f)

# Reinforcement Learning Environment
env = Environment(node, edge)

iteration = 0
while iteration < 3000:
    iteration += 1
    # 1st Ant tend to select a edge with stronger pheromones
    for key in Ant.keys():
        my_ant = Ant.get(key)
        my_node = my_ant.get('current_node')
        my_earlier_node = my_ant.get('earlier_node')
        my_edges = env.get_edges(my_node, my_earlier_node)

        my_pheromones = np.zeros(len(my_edges))
        my_pheromone_sum = 0.0
        for idx, edge in enumerate(my_edges):
            my_pheromone = my_edges[edge]['pheromone']
            my_pheromones[idx] = my_pheromone
            my_pheromone_sum += my_pheromone
        my_pheromones = my_pheromones / my_pheromone_sum
        my_edge = np.random.choice(list(my_edges.keys()), size=1, replace=False, p=my_pheromones)[0]
        my_ant['edge'] = my_edge

    # 2nd After the selection, ant add pheromones as it moves along the selected edge
    # 5th Ant that find food add additional pheromones based on the food
    alpha = 0.001
    for key in Ant.keys():
        my_ant = Ant.get(key)
        if my_ant['nest_to_food']:
            env.move(my_ant, alpha * 1.07)
        else:
            env.move(my_ant, alpha)

    # 3rd Ant pheromones evaporate itself naturally regardless of the ant's selection
    beta = 0.003
    env.pheromones_evaporate(beta)
    sum = 0
    for key in Ant.keys():
        my_ant = Ant.get(key)
        my_node = my_ant.get('current_node')
        if my_node in ['n0', 'n1', 'n2']:
            sum += 1
    percent = sum / len(Ant) * 100
    if iteration % 500 == 0:
        print(iteration, "{:3.2f}".format(percent), percent)

    # 4th Loop with 1st, 2nd, 3rd until find food
    # 6th Loop with 1st, 2nd, 3rd until return nest
    for reset_ant_key in Ant.keys():
        my_ant = Ant.get(reset_ant_key)
        if my_ant['food_to_nest']:
            my_ant['nest_to_food'] = False
            my_ant['food_to_nest'] = False

print('on developing')
