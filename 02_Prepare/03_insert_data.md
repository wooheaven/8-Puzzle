# insert data to database
```{bash}
$ docker cp 04_states.tsv pg:/states.tsv

$ docker exec -it pg /bin/bash

root@78779b6363ea:/# pwd
/

root@78779b6363ea:/# wc -l states.tsv
181440 states.tsv

root@78779b6363ea:/# ./mydatabase.sh
```
```{sql}

mydatabase=> CREATE TABLE states (       
    id             SERIAL      NOT NULL, 
    state          VARCHAR(9)  NOT NULL, 
    actions        VARCHAR(9)          , 
    next_states    VARCHAR(39)         , 
    next_ids       VARCHAR(27)         , 
    position       json                , 
    value_function json                  
) ;

mydatabase=> SELECT count(*) FROM states ;
 count
-------
     0
(1 row)

mydatabase=> \COPY states FROM '/states.tsv' DELIMITER E'\t' CSV HEADER ;
COPY 181440

mydatabase=> SELECT count(*) FROM states ;
 count
--------
 181440
(1 row)
```
