# container for database of 8-puzzle
```{bash}
$ docker pull postgres:13.0-alpine
docker images
REPOSITORY        TAG               IMAGE ID          CREATED             SIZE
postgres          13.0-alpine       65bf726222e1      9 days ago          160MB

docker run --name pg -p 65432:5432 -e POSTGRES_PASSWORD=123qwe -d postgres:13.6-alpine
7afa0980e8891935677718c9d006dc6b3d44070196ca444addaa30894b564709

docker ps
CONTAINER ID      IMAGE             COMMAND                  CREATED           STATUS            PORTS                    NAMES
7afa0980e889      postgres:10.4     "docker-entrypoint..."   10 seconds ago    Up 8 seconds      0.0.0.0:65432->5432/tcp  pg

docker exec -it pg /bin/bash

root@78779b6363ea:/# psql -h localhost -p 5432 -U postgres
psql (13.0)
Type "help" for help.

postgres=# CREATE ROLE myuser WITH LOGIN PASSWORD '123qwe' ;
CREATE ROLE

postgres=# \dg
                                   List of roles
 Role name |                         Attributes                         | Member of
-----------+------------------------------------------------------------+-----------
 myuser    |                                                            | {}
 postgres  | Superuser, Create role, Create DB, Replication, Bypass RLS | {}

postgres=# CREATE DATABASE mydatabase WITH OWNER = myuser ENCODING = 'UTF8' ;
CREATE DATABASE

postgres=# GRANT ALL privileges ON DATABASE mydatabase TO myuser ;
GRANT

postgres=# \l
                                  List of databases
    Name    |  Owner   | Encoding |  Collate   |   Ctype    |   Access privileges
------------+----------+----------+------------+------------+-----------------------
 mydatabase | myuser   | UTF8     | en_US.utf8 | en_US.utf8 | =Tc/myuser           +
            |          |          |            |            | myuser=CTc/myuser
 postgres   | postgres | UTF8     | en_US.utf8 | en_US.utf8 |
 template0  | postgres | UTF8     | en_US.utf8 | en_US.utf8 | =c/postgres          +
            |          |          |            |            | postgres=CTc/postgres
 template1  | postgres | UTF8     | en_US.utf8 | en_US.utf8 | =c/postgres          +
            |          |          |            |            | postgres=CTc/postgres
(4 rows)

postgres=# \q

root@78779b6363ea:/# echo "psql -h localhost -p 5432 -U myuser -d mydatabase" > mydatabase.sh
root@78779b6363ea:/# chmod +x mydatabase.sh
root@78779b6363ea:/# ./mydatabase.sh
psql (13.0)
Type "help" for help.

mydatabase=> \q
```
