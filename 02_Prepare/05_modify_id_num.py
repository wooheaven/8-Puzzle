import pandas as pd


if __name__ == "__main__":
    df = pd.read_csv("04_states.tsv", sep="\t")
    # for i, row in df.iterrows():
    #     print(i)
    #     print(row)
    for i, row in df.iterrows():
        # print(row)
        row["id"] = row["id"] - 1
        next_ids = ""
        for my_id in row["next_ids"].split(","):
            next_ids += str(int(my_id)-1) + ","
        next_ids = next_ids[:-1]
        row["next_ids"] = next_ids
        # print(row)
        df.iloc[i] = row
    df.to_csv("05_states.tsv", sep="\t", index=False, header=True)