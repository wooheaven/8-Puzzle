╠═1 Play  
║ ╠═1.1 [Example 1 : Shuffle, Optimal-Count](01_Play/01_example/index.html)  
║ ╠═1.2 [Example 2 : Initial, Goal, Search Algirhtm](01_Play/02_example/index.html)  
║ ╚═1.3 [Plan for Play](01_Play/03_Plan_for_Play.md)  
╠═2 Prepare  
║ ╠═2.1 [Create Database](02_Prepare/01_database_as_postgresql_on_docker.md)  
║ ╠═2.2 [Create data and Insert data to Database by Python](02_Prepare/02_create_data_and_insert_data.py)  
║ ╚═2.3 [Insert data to Database with file](02_Prepare/03_insert_data.md)  
╠═3 Tree Algorithm  
║ ╠═3.1 Solution  
║ ║ ╠═3.1.1 by SQL [id 30](03_Tree_Algorithm/01_Solution/01_by_SQL/01_8Puzzle_Tree_SQL_id30.sql), [id 78940](03_Tree_Algorithm/01_Solution/01_by_SQL/02_8Puzzle_Tree_SQL_id78940.sql)  
║ ║ ╚═3.1.2 by_Python  
║ ║ . ╠═3.1.2.1 [code](03_Tree_Algorithm/01_Solution/02_by_Python/01_by_Python.py)  
║ ║ . ╚═3.1.2.2 [result text](03_Tree_Algorithm/01_Solution/02_by_Python/02_result.txt)  
║ ╠═3.2 [Visualize : 30 Nodes Graph](03_Tree_Algorithm/02_Visualize/30node_graph.png)  
║ ╚═3.3 Wiki Reference  
║ . ╠═3.3.1 [Shortest-path tree](https://en.wikipedia.org/wiki/Shortest-path_tree)  
║ . ╚═3.3.2 [Spanning tree](https://en.wikipedia.org/wiki/Spanning_tree)  
╠═4 Dijkstra Algorithm  
║ ╠═4.1 [Reference](04_Dijkstra_Algorithm/01_Reference/)  
║ ╠═4.2 [with Label](04_Dijkstra_Algorithm/02_with_Label/01_Exam/exam_by_dijkstra_label.py)  
║ ║ ╠═4.2.1 [Exam](04_Dijkstra_Algorithm/02_with_Label/01_Exam/exam_by_dijkstra_label.py)  
║ ║ ╚═4.2.2 [30node](04_Dijkstra_Algorithm/02_with_Label/02_30node/30node_by_dijkstra_label.py)  
║ ╠═4.3 with Priority Que (not developed yet)  
║ ╚═4.4 Visualize  
║ . ╠═4.4.1 Gephi  
║ . ║ ╠═4.4.1.1 [181440 Node](04_Dijkstra_Algorithm/04_Visualize/01_Gephi/02_181440node/181440node.png)  
║ . ║ ╚═4.4.1.2 [30 Node](04_Dijkstra_Algorithm/04_Visualize/01_Gephi/03_30node/30node.png)  
║ . ╚═4.4.2 Plotly  
║ . . ╠═4.4.2.1 [Tree vs Reward](04_Dijkstra_Algorithm/04_Visualize/02_Plotly_Igraph/02_ployly_workspace/30node_tree_reward.html)  
║ . . ╚═4.4.2.2 [Reward vs 1st Iteration Value](04_Dijkstra_Algorithm/04_Visualize/02_Plotly_Igraph/02_ployly_workspace/30node_reward_value_01.html)  
╠═5 Dynamic Programming  
║ ╠═5.1 [Policy Iteration](05_Dynamic_Programming/01_Policy_Iteration/01_8Puzzle/policy_iteration.py)  
║ ║ ╠═5.1.1 Policy Evaluation  
║ ║ ║ ╠═5.1.1.1 [by Bellman Expectation Equation](05_Dynamic_Programming/01_Policy_Iteration/01_ValueFunctions_by_BEE.md)  
║ ║ ║ ╚═5.1.1.2 [by new Equation](05_Dynamic_Programming/01_Policy_Iteration/02_ValueFunctions_by_new.md)  
║ ║ ╠═5.1.2 Policy Improvement  
║ ║ ║ ╠═5.1.2.1 [Greedy Policy](05_Dynamic_Programming/01_Policy_Iteration/01_8Puzzle/01_Greedy_Policies/06_Greedy_Policy_Iteration.png)  
║ ║ ║ ╠═5.1.2.2 [Softmax Policy](05_Dynamic_Programming/01_Policy_Iteration/01_8Puzzle/02_Softmax_Policies/06_Softmax_Policy_Iteration.png)  
║ ║ ║ ╠═5.1.2.3 [Asis Policy](05_Dynamic_Programming/01_Policy_Iteration/01_8Puzzle/03_Asis_Policies/06_Asis_Policy_Iteration.png)  
║ ║ ║ ╚═5.1.2.4 [New Policy](05_Dynamic_Programming/01_Policy_Iteration/01_8Puzzle/04_New_Policy_Evaluation/06_New_Policy_Evaluation.png)  
║ ║ ╚═5.1.3 [Compare Shortest Path for Greedy, Softmax, Asis, New](05_Dynamic_Programming/01_Policy_Iteration/03_Compare_Shortest_Path_by_Policies.html)  
║ ╠═5.2 [Value Iteration](05_Dynamic_Programming/02_Value_Iteration/)  
║ ║ ╠═5.2.1 [BEE vs BOE](05_Dynamic_Programming/02_Value_Iteration/01_BEE_vs_BOE.md)  
║ ║ ╚═5.2.2 [Policy Iteration vs Value Iteration](05_Dynamic_Programming/02_Value_Iteration/02_Policy_Iteration_vs_Value_Iteration.md)  
║ ╚═5.3 [Weakness of DP](05_Dynamic_Programming/01_Weakness_of_Dynamic_Programming.md)  
╠═6 Reinforcement Learning  
║ ╠═6.1 [Compare RL & DP](06_Reinforcement_Learning/01_Compare_RL_and_DP.md)  
║ ╠═6.2 Monte Carlo Prediction  
║ ║ ╠═6.2.1 [Monte Carlo Method](https://ko.wikipedia.org/wiki/몬테카를로_방법)  
║ ║ ╚═6.2.2 [Monte Carlo Prediction](06_Reinforcement_Learning/02_Monte_Carlo_Prediction/01_Monte_Carlo_Prediction.md)  
║ ╠═6.3 [Temporal Difference Prediction](06_Reinforcement_Learning/03_Temporal_Difference_Prediction/01_TDP_vs_MCP.md)  
║ ╠═6.4 SARSA  
║ ║ ╠═6.4.1 [Apply TDP to DP](06_Reinforcement_Learning/04_SARSA/01_SARSA.md)  
║ ║ ╚═6.4.2 [Weakness](06_Reinforcement_Learning/04_SARSA/02_SARSA_Weakness.md)  
║ ╠═6.5 QLearning  
║ ║ ╚═6.5.1 [Policy for learn and action](06_Reinforcement_Learning/05_QLearning/01_QLearning.md)  
║ ║ . ╚═6.5.1.1 [8-Puzzle Implement](06_Reinforcement_Learning/05_QLearning/01_8Puzzle/8Puzzle_by_QLearning.py)  
║ ╠═6.6 Deep SARSA  
║ ║ ╚═6.6.1 [Why add Deep to RL (SARSA)](06_Reinforcement_Learning/06_DeepSARSA/01_DeepSARSA.md)  
║ ║ . ╚═6.6.1.1 [8-Puzzle Implement](06_Reinforcement_Learning/06_DeepSARSA/01_8Puzzle/8Puzzle_by_DeepSARSA.py)  
║ ╠═6.7 Policy Gradient  
║ ║ ╚═6.7.1 [Based on Policy not Value](06_Reinforcement_Learning/07_Policy_Gradient/01_Policy_Gradient.md)  
║ ║ . ╠═6.7.1.1 [8-Puzzle Implement](06_Reinforcement_Learning/07_Policy_Gradient/01_8Puzzle/8Puzzle_by_PolicyGradient.py)  
║ ║ . ╚═6.7.1.2 [8-Puzzle Run on Colab](06_Reinforcement_Learning/07_Policy_Gradient/02_Policy_Gradient_on_Colab.md)  
║ ╚═6.8 [Flow of MDP](06_Reinforcement_Learning/08_Gliffy_Workspace/01_Flow_of_MDP.png)  
╠═7 Swarm Intelligence  
║ ╚═7.1 Ant Colony Optimization  
║ . ╠═7.1.1 Ref  
║ . ║.╠═7.1.1.1 [ACO on Wikipedia](https://en.wikipedia.org/wiki/Ant_colony_optimization_algorithms)  
║ . ║.╠═7.1.1.2 [ACO Korean Blog](http://antalg.egloos.com/v/82254)  
║ . ║.╚═7.1.1.3 [Ant Colony Optimization : Introduction and recent trends](07_Swarm_Intelligence/01_Ant_Colony_Optimization/01_Reference/01_antcolonyopt.pdf)  
║ . ╚═7.1.2 Simple ACO  
║ . . ╚═7.1.2.1 [Simple ACO : on developing](07_Swarm_Intelligence/01_Ant_Colony_Optimization/02_Simple_ACO/ACO.py)  
╚═8 Reference  
 . ╠═8.1 [KSC 2018](http://www.ksc2018.re.kr/about03.html)  
 . ║ ╠═8.1.1 [바둑 카카오 AI vs 신민준](08_Reference/01_KSC_2018/KakaoTalk_Image_2019-06-05-16-26-24.jpeg)  
 . ║ ╠═8.1.2 [AlphaGo and Next](http://www.ttimes.co.kr/view.html?no=2019041917377740752)  
 . ║ ╚═8.1.3 [알파오목](08_Reference/01_KSC_2018/KakaoTalk_Image_2019-06-05-16-28-43.jpeg)  
 . ╠═8.2 Player and Role  
 . ║ ╠═8.2.1 [AI Player and Role](https://github.com/Kyubyong/dl_career_faq)  
 . ║ ╚═8.2.2 [8-Puzzle Player and Role](08_Reference/02_AI_Player_and_Role/01_8-Puzzle_Player_and_Role.png)  
 . ╚═8.3 Compare and OverView of 8-Puzzle Algorithms  
 . . ╠═8.3.1 [Compare Algorithm's Concept](08_Reference/03_Compare_and_OverView/01_Compare_8-Puzzle_Implements.png)  
 . . ╚═8.3.2 [8-Puzzle's OverView](08_Reference/03_Compare_and_OverView/02_8-Puzzle_OverView.png)  
( ║ ╠ ═ ╚ )  

Research-Process  
1 Play 8-Puzzle  
2 Prepare Data  
4 Dijkstra Algorithm -> Visualize  
3 Tree Algorithm  
4 Dijkstra Algorithm -> Implement
5 Dynamic Programming -> Policy Iteration, Value Iteration  
6 Reinforcement Learning ->  Monte Carlo Prediction, Temporal Difference Prediction, Temporal Difference Control = SARSA, QLearning, Deep SARSA, Policy Gradient  
