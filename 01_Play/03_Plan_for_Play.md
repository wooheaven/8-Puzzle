# Plan to create 8-Puzzle Environment(Game)
```{text}
requirements
When  : After Spring, Svelte Self-study
Where : My Notebook
Who   : wooheaven
How   : Local Self-Hosting
Why   : To visualize 8-Puzzle
What  : 8-Puzzle on Web

# Merge 2 Sliding Examples  
1 Shuffle  
2 Print Game progress(Monitor) :  
  for example : 0 moves so for, Reward(Distance)=18,  
3 Solve  

# REST full 8-Puzzle Environment(Web based Game)  
4 Dependent Process for 8-Puzzle to Independent one  
5 Plot Game Progress based on Graph Theory  
```
