# 일반 정책 이터레이션 Generailzed Policy Iteration (GPI)  
```{text}
DP에 TDP를 적용하기 전에 PI를 일반화 하자.  

GPI는 PI의 일반화로써 
Policy Iteration의 Policy Evaluation과 Policy Improvement를  
한 번씩 번갈아 가면서 실행하여
ValueFunction이 Optimal ValueFunction에 수렴하도록 한다.  

PI에서는 PE을 여러 번씩 실행하면 True ValueFunction으로 수렴하지만  
GPI에서는 PE+PI를 여러 번씩 실행하면 Optimal ValueFunction으로 수렴한다.  
```

# 시간차 제어 Temporal-Difference control (TDC)  
```{text}
DP에 TDP를 적용해보자.  

TDP를 PI에 적용한다면, 아래와 같은 문제가 발생한다.  
  Policy Evaluation의 관점에서는  
    GPI에서는 모든 State s에 대하여 주어진 ValueFunction을 update한다.
    TDP에서는 time step마다 주어진 ValueFunction을 update한다.
  Policy Improvement의 관점에서는
    GPI의 Greedy PI은 주어진 가치함수로 모든 상태에 대하여 PI한다.  
    TDP는 GPI에서 처럼 모든 상태에 대하여 PI를 할수 없다.  

TDP를 VI에 적용한다면, 가능하고,  
  TDP과 Greedy Policy를 합친 것을  
  Temporal-difference control TDC라고 한다.
```

# State Action Reward State Action (SARSA)  
GPI에서 Greedy Policy Improvement를 살펴보면, 아래와 같다.  
$` \pi'(s) = argmax_{a \in A} [R_s^a + \gamma \cdot P_{ss'}^a \cdot V(s') ] `$  

그런데 TDC에서 위의 수식을 Greedy Policy로 선택할 수 없다.  
왜냐하면 Environment의 Model인 $` P_{ss'}^a `$를 알수 없기 때문이다.  

QFunction을 이용한 Greedy Policy를 살펴보면, 아래와 같다.  
$` \pi(s) = argmax_{a \in A} Q(s, a) `$  

TDC에서 위의 수식을 Greedy Policy로 선택가능하고  
이에 따라서 TDC에서는 ValueFunction이 아니라 QFunction을 Update하여 Policy Evaluation을 할수 있고, 그 식은 아래와 같다.  
$` Q(S_t, A_t) \leftarrow Q(S_t, A_t) + \alpha \cdot [R + \gamma \cdot Q(S_{t+1}, A_{t+1}) - Q(S_t, A_t)] \; where \; A_{t+1} = \pi(S_{t+1}) `$  

TDC의 과정을 살펴보자.  
여기서 Agent는 시작 상태 $` S_t `$에서  
Greedy Policy에 따라서 행동 $` A_t `$를 선택하여  
Env에서 Time Step 한번을 진행하고  
Env는 Agent에게 보상 $` R_{t+1} `$과 다은 상태 $` S_{t+1} `$를 넘겨준다.  
여기서 Agent는 Greedy Policy에 따라서 행동 $` A_{t+1} `$을 선택한다.  
위의 과정으로 $` [S_t, A_t, R_{t+1}, S_{t+1}, A_{t+1}] `$ 가 생성되면 하나의 Sample로써 QFunction을 Update 할 수 있다.  
이런 의미에서 TDC를 SARSA라고 한다.  

# ϵ Greedy Policy  
초기의 Agent는 Greedy Policy의 부족한 학습으로 Optimal하지 않다.  
즉 충분한 경험이 필요한데,  
ϵ만큼의 확률로 Greedy Policy를 따르지 않는 선택을 하게한다.  
이를 ϵ Greedy Policy라고 한다.  
여기서 ϵ이 Exploration이고, 1-ϵ이 Exploitation이다.

이를 식으로 정리하면 아래와 같다.  
$` \pi(s) = \begin{cases} 
            a^{*} = argmax_{a \in A} Q(s, a) &\text{for } 1- \epsilon \\
            a \not = a^{*} &\text{for } \epsilon
\end{cases} `$  

# SARSA vs GPI  
```{text}
GPI   depends on PI.
SARSA depends on VI.

In GPI,   use PE  for update ValueFunction.
In SARSA, use TDP for update QFunction.

In GPI,   use Greedy Policy Improvement for update Policy.
In SARSA, use ϵ Greedy Policy           for update Policy.
```

# [Flow of SARSA](../08_Gliffy_Workspace/04_SARSA.png) 
