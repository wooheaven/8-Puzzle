# Weakness of SARSA  
```{text}
SARSA에서는 ϵ Greedy Policy로 학습을 하는데,  
Agent가 행동하는대로 학습하는 시간차 제어이다.  
이를 On-Policy Temporal-Difference Control이라고 한다.  
여기서 단점이 발생하는데, Optimal Policy에 근사해야 하는데,  
Agent가 행동하는 방식으로만 학습되고, 행동하지 않은 방식은 학습되지 않는다.  
즉 Agent가 자신의 행동에 편향되어 학습한다는 것이 단점이다.  
```