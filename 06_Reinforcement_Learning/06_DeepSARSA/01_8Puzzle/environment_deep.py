import numpy as np
import copy
import json

class Env():
    def __init__(self, reward_file):
        self.state = None
        print('reward_dict creation\t\t: from file [' + str(reward_file) + '] is started')
        with open(file=reward_file) as read_file:
            self.reward_dict = json.load(fp=read_file)
        print('reward_dict creation\t\t: from file [' + str(reward_file) + '] is ended')

    def step(self, state, action):
        # next_state
        zero_index = self.find_zero(state)
        move_index = self.find_move(zero_index, action)
        next_state = copy.deepcopy(state)
        next_state[0][zero_index], next_state[0][move_index] = next_state[0][move_index], next_state[0][zero_index]
        # reward
        next_state_str = self.get_str_from_array(next_state[0])
        reward = self.reward_dict.get(next_state_str)
        # done
        done = (next_state_str == '123456780')
        return reward, next_state, done

    def find_zero(self, state):
        zero = np.float64(0.)
        for i in range(len(state[0])):
            if zero == state[0][i]:
                break
        return i

    def find_move(self, zero_index, action):
        zero_matrix = ((int) (zero_index/3), (int) (zero_index%3))
        if action == 0:
            if zero_matrix[0] == 0:
                move_matrix = (zero_matrix[0], zero_matrix[1])
            else:
                move_matrix = (zero_matrix[0]-1, zero_matrix[1])
        elif action == 1:
            if zero_matrix[0] == 2:
                move_matrix = (zero_matrix[0], zero_matrix[1])
            else:
                move_matrix = (zero_matrix[0]+1, zero_matrix[1])
        elif action == 2:
            if zero_matrix[1] == 0:
                move_matrix = (zero_matrix[0], zero_matrix[1])
            else:
                move_matrix = (zero_matrix[0], zero_matrix[1]-1)
        elif action == 3:
            if zero_matrix[1] == 2:
                move_matrix = (zero_matrix[0], zero_matrix[1])
            else:
                move_matrix = (zero_matrix[0], zero_matrix[1]+1)
        move_index = move_matrix[0] * 3 + move_matrix[1]
        return move_index

    def get_str_from_array(self, next_state):
        result = ""
        for e in next_state:
            result += str(int(e))
        return result