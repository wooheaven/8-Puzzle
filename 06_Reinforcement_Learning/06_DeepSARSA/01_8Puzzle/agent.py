from keras.layers import Dense
from keras.models import Sequential
from keras.optimizers import Adam
import numpy as np

class Agent():
    def __init__(self, load_model):
        # 0 Up
        # 1 Down
        # 2 Left
        # 3 Right
        self.actions = [0, 1, 2, 3]
        self.actions_size = len(self.actions)
        # array([[1, 2, 3],
        #        [4, 5, 6],
        #        [7, 8, 0]])
        # array([1, 2, 3, 4, 5, 6, 7, 8, 0])
        self.state_size = 9

        self.discount_factor = 0.99
        self.learning_rate = 0.01
        self.epsilon = 1.
        self.epsilon_decay = 0.9
        self.epsilon_min = 0.01
        self.model = self.build_model()
        self.load_model = load_model

        if self.load_model:
            self.model.load_weights('./save_model/deep_sarsa.h5')

    def build_model(self):
        model = Sequential()
        model.add(Dense(30, input_dim=self.state_size, activation='relu'))
        model.add(Dense(30, activation='relu'))
        model.add(Dense(self.actions_size, activation='linear'))
        model.summary()
        model.compile(loss='mse', optimizer=Adam(lr=self.learning_rate))
        return model

    def get_action(self, state):
        validate = False
        while validate is False:
            if np.random.rand() < self.epsilon:
                action = np.random.choice(self.actions, size=1)[0]
            else:
                q_values = self.model.predict(state)
                action = np.argmax(q_values[0])
            validate = self.validate_action(state, action)
            if validate is False:
                target = self.model.predict(state)[0]
                target[action] *= 0.1
        return action

    def validate_action(self, state, action):
        zero_index = self.find_zero(state)
        zero_xpos = zero_index / 3
        zero_ypos = zero_index % 3
        validate = True
        if zero_xpos == 0 and action == 0:
            validate = False
        elif zero_xpos == 2 and action == 1:
            validate = False
        elif zero_ypos == 0 and action == 2:
            validate = False
        elif zero_ypos == 2 and action == 3:
            validate = False
        return validate

    def find_zero(self, state):
        zero = np.float64(0.)
        for i in range(len(state[0])):
            if zero == state[0][i]:
                break
        return i

    def train_model(self, state, action, reward, next_state, next_action, done):
        target = self.model.predict(state)[0]
        if done:
            target[action] = reward
        else:
            target[action] = reward + self.discount_factor * self.model.predict(next_state)[0][next_action]
        target = np.reshape(target, (1, 4))
        self.model.fit(state, target, epochs=1, verbose=0)

    def reduce_epsilon(self):
        if self.epsilon > self.epsilon_min:
            self.epsilon *= self.epsilon_decay
