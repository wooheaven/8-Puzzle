from environment_deep import Env
from agent import Agent
import numpy as np
import copy
import sys
import matplotlib
matplotlib.use('Agg')
import pylab


def print_status(episode, state, action, reward, next_state, next_action, global_move, bar_length=20):
    arrow = '-' * int(round(reward / 1.0 * bar_length) - 1) + '>'
    spaces = ' ' * (bar_length - len(arrow))
    sys.stdout.write("\rbar=[{0}], E={1}, S={2}, A={3}, R={4:1.2f}, nextS={5}, nextA={6}, step={7}"
                     .format(arrow + spaces,
                             episode, state, action, reward, next_state, next_action, global_move))
    sys.stdout.flush()


if __name__ == "__main__":
    env = Env('reward.json')
    agent = Agent(load_model=True)

    scores, episodes = [], []
    episode_size = 150
    print("episode,S,A,R,S',A',step")

    for episode in range(episode_size):
        done = False
        score = 0
        initial_state = np.reshape([1., 2., 0., 4., 5., 3., 7., 8., 6.], (1, 9))
        state = copy.deepcopy(initial_state)
        global_step = 0

        while not done:
            global_step += 1

            action = agent.get_action(state)
            reward, next_state, done = env.step(state, action)
            next_action = agent.get_action(next_state)
            print_status(episode, state, action, reward, next_state, next_action, global_step)
            agent.train_model(state, action, reward, next_state, next_action, done)

            state = copy.deepcopy(next_state)
            score += reward

            if global_step == sys.maxsize:
                global_step = 0
                agent.model.save_weights("./save_model/deep_sarsa.h5")
                print("global_step overflow happend")

        scores.append(score)
        episodes.append(episode)
        pylab.plot(episodes, scores, 'b')
        pylab.savefig("./save_graph/deep_sarsa.png")
        print("\nepisode: ", episode, "  score: ", score, "  step: ", global_step, "  epsilon: ", agent.epsilon)
        agent.reduce_epsilon()
        agent.model.save_weights("./save_model/deep_sarsa.h5")

