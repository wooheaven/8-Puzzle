# Dynamic Programming's Weakness
```{text}
1 Complex Compute
2 Curse of dimensionality
3 Perfect information of Environment
```

# What Reinforcement Learning covers over Dynamic Programming's weakness
```{text}
In this tutorial, 
Reinforcement Learning consists of SARSA, QLearning.  
Reinforcemnet Learning can learn by sampling without model of environment.  
We call this above property as model-free.
So, RL covers [3 Perfect information of Environment]
For example, on 8-Puzzle
We don't have to prepare 181,440 states for iteration compute on Dynamic Programming.
Rather than above, we use episode on Reinforcement Learning.
```

# Reinforcement Learning to Deep Learning
```{text}
When Reinforcement Learning loading or updating QFunction,
I need to QFunction as a table.
Actually, it is not necessary to return a output for every inputs.
If we have a function which act like human's abstraction ability,
then we can covers 1 and 2.
Deep Learning is a solution of the above sentence.
Deep Learning use Artificail Nueral Networks like RL use QFunction Table  
to return a output for every inputs.
In other words, Deep Learning use ANN as a Approximator to replace QFunction.
```
