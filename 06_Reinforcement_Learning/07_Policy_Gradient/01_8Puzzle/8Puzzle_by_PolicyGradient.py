from environment_PolicyGradient import Env
from agent_PolicyGradient import Agent
import numpy as np
import copy
import sys
import matplotlib
matplotlib.use('Agg')
import pylab


def print_status(episode, global_step, state, action, reward, next_state, bar_length=10):
    arrow = '-' * int(round(reward / 1.0 * bar_length) - 1) + '>'
    spaces = ' ' * (bar_length - len(arrow))
    sys.stdout.write("\rbar=[{0}], E={1}, S={2}, A={3}, R={4:1.2f}, nS={5}, step={6}".format(arrow + spaces, episode, state, action, reward, next_state, global_step))
    sys.stdout.flush()


if __name__ == "__main__":
    env = Env('reward.json')
    agent = Agent(load_model=True)

    global_step = 0
    scores, episodes = [], []
    episode_size = 2500
    initial_state = np.reshape([1., 2., 3., 4., 5., 0., 7., 8., 6.], [1, 9])

    for episode in range(episode_size):
        done = False
        score = 0
        state = copy.deepcopy(initial_state)
        episode_states, episode_actions, episode_rewards = [], [], []
        episode_tuples = (episode_states, episode_actions, episode_rewards)

        while not done:
            global_step += 1
            action = agent.get_action(state)
            reward, next_state, done = env.step(state, action)

            agent.append_sample(episode_tuples, state, action, reward)
            score += reward
            print_status(episode, global_step, state, action, reward, next_state)
            state = copy.deepcopy(next_state)

            if global_step == sys.maxsize:
                global_step = 0
                agent.model.save_weights("./save_model/reinforce.h5")
                print("global_move overflow happend")

        agent.train_model(episode_tuples)
        scores.append(score)
        episodes.append(episode)
        print("\nepisode: ", episode, "\tscore: ", round(score, 2), "\tglobal_step: ", global_step, "\n")
        pylab.plot(episodes, scores, 'b')
        pylab.savefig("./save_graph/reinforce.png")
        agent.model.save_weights("./save_model/reinforce.h5")
        agent.reduce_epsilon()
