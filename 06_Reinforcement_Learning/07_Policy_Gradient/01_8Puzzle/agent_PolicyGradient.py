from keras.layers import Dense
from keras.models import Sequential
import numpy as np
from keras import backend as K
from keras.optimizers import  Adam


class Agent():
    def __init__(self, load_model):
        # 0 Up
        # 1 Down
        # 2 Left
        # 3 Right
        self.actions = [0, 1, 2, 3]
        self.actions_size = len(self.actions)
        # array([[1, 2, 3],
        #        [4, 5, 6],
        #        [7, 8, 0]])
        # array([1, 2, 3, 4, 5, 6, 7, 8, 0])
        self.state_size = 9
        self.model = self.build_model()
        if load_model:
            # self.model.load_weights('./save_model/random.h5')
            self.model.load_weights('./save_model/reinforce.h5')
        self.discount_factor = 0.99
        self.learning_rate = 0.001
        self.epsilon = 0.5
        self.epsilon_decay = 0.9
        self.epsilon_min = 0.01
        self.optimizer = self.optimizer()

    def build_model(self):
        model = Sequential()
        model.add(Dense(24, input_dim=self.state_size, activation='relu'))
        model.add(Dense(24, activation='relu'))
        model.add(Dense(self.actions_size, activation='softmax'))
        model.summary()
        return model

    def optimizer(self):
        action = K.placeholder(shape=[None, self.actions_size])
        discounted_rewards = K.placeholder(shape=[None, ])

        action_prob = K.sum(action * self.model.output, axis=1)
        cross_entropy = K.log(action_prob) * discounted_rewards
        loss = -K.sum(cross_entropy)

        optimizer = Adam(lr=self.learning_rate)
        updates = optimizer.get_updates(self.model.trainable_weights, [], loss)
        train = K.function([self.model.input, action, discounted_rewards], [], updates=updates)
        return train

    def get_action(self, state):
        validate = False
        while validate is False:
            if np.random.rand() < self.epsilon:
                action = np.random.choice(self.actions_size, 1)[0]
            else:
                policy = self.model.predict(state)[0]
                action = np.random.choice(self.actions_size, 1, p=policy)[0]
            validate = self.validate_action(state, action)
            if validate is False:
                episode_states = list()
                episode_states.append(state[0])
                act = np.zeros(self.actions_size)
                act[action] = 1
                episode_actions = list()
                episode_actions.append(act)
                reward = [-1.0]
                episode_rewards = np.zeros_like(reward)
                episode_rewards[0] += reward[0]
                episode_rewards = np.float32(episode_rewards)
                self.optimizer([episode_states, episode_actions, episode_rewards])
        return action

    def validate_action(self, state, action):
        zero_index = self.find_zero(state)
        zero_x = zero_index // 3
        zero_y = zero_index % 3
        validate = True
        if zero_x == 0 and action == 0:
            validate = False
        elif zero_x == 2 and action == 1:
            validate = False
        elif zero_y == 0 and action == 2:
            validate = False
        elif zero_y == 2 and action == 3:
            validate = False
        return validate

    def find_zero(self, state):
        zero = np.float64(0.)
        for i in range(len(state[0])):
            if zero == state[0][i]:
                break
        return i

    def append_sample(self, episode_tuples, state, action, reward):
        episode_tuples[0].append(state[0])
        act = np.zeros(self.actions_size)
        act[action] = 1
        episode_tuples[1].append(act)
        episode_tuples[2].append(reward)

    def train_model(self, episode_tuples):
        discounted_rewards = np.float32(self.discount_rewards(episode_tuples[2]))
        if len(discounted_rewards) > 1: # avoid to divide zero STD
            discounted_rewards -= np.mean(discounted_rewards)
            discounted_rewards /= np.std(discounted_rewards)
        self.optimizer([episode_tuples[0], episode_tuples[1], discounted_rewards])

    def reduce_epsilon(self):
        if self.epsilon > self.epsilon_min:
            self.epsilon *= self.epsilon_decay

    def discount_rewards(self, episode_rewards):
        discounted_rewards = np.zeros_like(episode_rewards)
        running_add = 0
        for i in reversed(range(0, len(episode_rewards))):
            running_add = episode_rewards[i] + self.discount_factor * running_add
            discounted_rewards[i] = running_add
        return discounted_rewards
