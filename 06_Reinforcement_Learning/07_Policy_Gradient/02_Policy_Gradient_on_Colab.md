# Compress Run files
```{bash}
$ pwd
/home/ubuntu/Documents/09_8-Puzzle_WorkSpace/8-Puzzle/06_Reinforcement_Learning/07_Policy_Gradient

$ tar -zcf 8.tar.gz 01_8Puzzle/
```

# Sync with Google Drive & Colab
```{text}
!apt-get install -y -qq software-properties-common python-software-properties module-init-tools
!add-apt-repository -y ppa:alessandro-strada/ppa 2>&1 > /dev/null
!apt-get update -qq 2>&1 > /dev/null
!apt-get -y install -qq google-drive-ocamlfuse fuse
from google.colab import auth
auth.authenticate_user()

from oauth2client.client import GoogleCredentials
creds = GoogleCredentials.get_application_default()

import getpass
!google-drive-ocamlfuse -headless -id={creds.client_id} -secret={creds.client_secret} < /dev/null 2>&1 | grep URL

vcode = getpass.getpass()
!echo {vcode} | google-drive-ocamlfuse -headless -id={creds.client_id} -secret={creds.client_secret}
```

# Link to URL for verification code

# Mount [Colab's /content/drive folder] to [Google Drive's root folder]
```{text}
!pwd

!mkdir -p drive
!google-drive-ocamlfuse drive
```

# Run RL Policy Gradient
```{text}
!cd drive/'Colab Notebooks'; ls -al;

!cd drive/'Colab Notebooks'; tar -zxf 8.tar.gz; ls -al;

!cd drive/'Colab Notebooks'/01_8Puzzle; ls -al;

!cd drive/'Colab Notebooks'/01_8Puzzle; python 8Puzzle_by_PolicyGradient.py ;
```

# Ref
```{text}
https://zzsza.github.io/data/2018/08/30/google-colab/
```
