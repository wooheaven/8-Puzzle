# Monte Carlo Prediction
```{text}
Episode란 Agent가 직접 Policy를 따라서 행동을 하면서 보상을 받는 것이다.  
8-Puzzle에서는 한번의 퍼즐을 푸는 것이 하나의 Episode이다.  
끝이 없는 Episode는 Return을 알기 어려우므로 모든 Episode는 끝이 있다고 가정한다.  
유한번의 Action으로 Episode가 끝난다면 각 State마다의 Return을 계산할 수 있다.  
Sampling이란 Agent가 Environment에서 한번의 Episode를 실행하는 것이다.  
Agent가 여러 Sampling을 통해 평균으로 True ValueFunction을 predict(learn) 하는 것이다.  
```

# 계산 가능한 ValueFunction by BEE
k번째 iteration의 (계산된) ValueFunction을 알고 있다고 하자.  
그러면 k+1번째 iteration의 (계산된) ValueFunction을 다음과 같이 Update할 수 있다.  
$`v_{\pi, k+1}(S_t) = \displaystyle\sum_{a \in A} {\pi}(a|S_t) \cdot \{ R_{t+1} + \gamma \cdot v_{\pi, k}(S_{t+1}) \}`$  
(by alias) $` s = S_t, s' = S_{t+1}, Policy \: \pi `$  
$`v_{k+1}(s) = \displaystyle\sum_{a \in A} {\pi}(a|s) \cdot \{ R_{t+1} + \gamma \cdot v_{k}(S_{s'}) \}`$  
input : $`v_{k}(s'), \: R_{t+1}, \: {\pi}(a|s)`$  
output : $`v_{k+1}(s)`$  


# 계산 가능한 ValueFunction by MCP
n번째 episode의 (추정된) ValueFunction을 알고 있다고 하자.  
그러면 n+1번째 episode의 (추정된) ValueFunction을 다음과 같이 Update할 수 있다.  

$`G_i(S_t)`$ is the Return of State $`S_t`$ during $`i`$th episode  
$`N(S_t)`$ is the number of times the agent visited the state $`S_t`$ during episodes  

$`v_{\pi, n+1}(S_t) = 1/N(S_t) \cdot \displaystyle\sum_{i = 1}^{N(S_t)} G_i(S_t) `$  
$`\kern4em\: = 1/n \cdot \displaystyle\sum_{i = 1}^n G_i `$ (by alias) $`G_i = G_i(S_t)`$  
$`\kern4em\: = 1/n \cdot (G_n + \displaystyle\sum_{i = 1}^{n-1} G_i ) `$  
$`\kern4em\: = 1/n \cdot (G_n + (n-1) \cdot 1/(n-1) \cdot \displaystyle\sum_{i = 1}^{n-1} G_i ) `$  
$`\kern4em\: = 1/n \cdot (G_n + (n-1) \cdot v_{\pi, n}(S_t) ) `$  
$`\kern4em\: = v_{\pi, n}(S_t) + 1/n \cdot (G_n - v_{\pi, n}(S_t) ) `$  
(by alias) $`s = S_t, Policy \: \pi `$  
$`v_{n+1}(s) = v_{n}(s) + 1/n \cdot (G_n - v_{n}(s) ) `$  
$`v_{n+1}(s) = v_{n}(s) + \alpha \cdot (G_n - v_{n}(s) ) `$  
input : $`v_{n}(s), \: G(s), \: \alpha`$  
output : $`v_{n+1}(s)`$  

[MCP vs DP : Update ValueFunction](06_Reinforcement_Learning/06_Gliffy_Workspace/02_Monte_Carlo_Prediction.png)  

# Impossible to apply MCP to 8-Puzzle
```{text}
유한번의 Action으로 Random하게 Puzzle을 움직여서 State 0번에 도착하는 Episode를 만들기 매우 힘들다.
확률적으로 0%가 아니더라도 직접 구현해서 Episode를 만들기 매우 어렵다.
```