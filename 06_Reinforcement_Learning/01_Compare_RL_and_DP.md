# RL vs DP by ValueFunction & Policy
```
I feel good/bad about states then I select action for better state.
What happend if I make the Agent follows me?

In DP, the Agent calculate the true ValueFunction based on given all-states and policy.  
In RL, the Agent predict(learn) the true ValueFunction by it's own experienced-states and policy.(through interaction with the environment)  

So DP don't find states.
So RL must experience states.

In DP, we call Policy Iteration 
       be the sum of Policy_Evaluation and Policy_Improvement.
In RL, we call Control 
       be the sum of prediction and Policy_Improvement.  
```

# RL vs DP by graph
```mermaid
graph TB
A[DP] --> |given all states|C[States]
B[RL] --> |experience each states|C
C --> |Calculate|E[ValueFunction]
C --> |predict or learn|E
E --> |Improve|D[Policy]
```

# RL vs DP by programming paradigm
```mermaid
graph TB
A[Real World] -->|Focus on|B(Environment)
B --> |White or Black Box|C(Relation between in & out)
C --> |parameterize|D(Equation)
D --> |Based on| E(Model)
E --> F(Error)
F --> |Experiment & Tune| E
C --> |Learn|G(RL)
```
