# QLearning  
1989년 Chris Watkins에 의하여 소개된 RL Algorithm 이다. 

In SARSA  
next state $` s' `$ 에서  
ϵ Greedy Policy $` \pi `$에 따라서 next action $` a' `$를 선택하고,  
$` s', a' `$을 Sample에 포함하여 QFunction을 업데이트한다.  
(On Policy)  

In QLearning  
next state $` s' `$ 에서  
그 값이 가장 큰 $` max_{a'}Q(S_{t+1}, a') `$를 선택하고,  
$` s' `$을 Sample에 포함하여 QFunction을 업데이트한다.  
(Off Policy)  

# Apply [BEE] to [QFunction Update by SARSA]
$` v_{\pi}(S_t) = E_{\pi} [ R_{t+1} + \gamma \cdot v_{\pi}(S_{t+1}) \enspace | \enspace S_t = s ]  `$  
$` Q(S_t, A_t) \leftarrow Q(S_t, A_t) + \alpha \cdot [R + \gamma \cdot Q(S_{t+1}, A_{t+1}) - Q(S_t, A_t)] \;\; where \; A_{t+1} = \pi(S_{t+1}) `$  

# Apply [BOE] to [QFunction Update by QLearning]
$` q_{*}(s,a) = E [ R_{t+1} + \gamma \cdot \max\limits_{a'} q_{*}(s', a') \: | \: S_{t} = s, \: S_{t+1} = s' \: A_{t} = a, \: A_{t+1} = a' ] `$  
$` Q(S_t, A_t) \leftarrow Q(S_t, A_t) + \alpha \cdot [R_{t+1} + \gamma \cdot \max\limits_{A_{t+1}}Q(S_{t+1}, A_{t+1}) - Q(S_t, A_t)] `$  