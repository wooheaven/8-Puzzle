from QLearning import QLearning

if __name__ == '__main__':
    # arg = {'qfunction_file' : 'random_qfunction.json.gz',
    arg = {'qfunction_file' : 'qfunction.json.gz',
           'reward_file' : 'reward_by_manhattan_distance.json'}

    episode_size = 1001
    initial_state = 1
    learning = QLearning(**arg)
    learning.print_qlearning_head()
    for episode in range(1, episode_size):
        learning.qlearning(episode, initial_state)
        learning.save_qfunction('qfunction.json.gz')

    # print qfunction
    # sample_episodes = range(10)
    # sample_ids = range(10)
    # learning.print_qfunction(sample_ids)
