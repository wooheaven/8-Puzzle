from collections import defaultdict
import json
import gzip
import sys
import numpy as np

class QDict:
    def __init__(self, qfunction_file):
        print('qfunction creation\t: from file [' + str(qfunction_file) + '] is started')
        with gzip.GzipFile(filename=qfunction_file, mode='r') as read_file:
            self.qfunction = json.loads(read_file.read().decode(encoding='utf-8'))
        # with open(file=qfunction_file) as read_file:
        #     self.qfunction = json.load(fp=read_file)
        print('qfunction creation\t: from file [' + str(qfunction_file) + '] is ended')

    def get_action_value_dict(self, state):
        self.state_key_exception(state)
        result = defaultdict(float)
        for action in self.qfunction['0'][state].keys():
            result[action] = self.get_value(state, action)
        return result

    def get_value(self, state, action, version=None):
        self.key_exception(state, action)
        if version is None:
            version = self.get_last_version(state, action)
        return self.qfunction['0'][state][action][version]

    def set_value(self, state, action, value):
        self.key_exception(state, action)
        previous_version = self.get_last_version(state, action)
        new_version = str(int(previous_version) + 1)
        self.qfunction['0'][state][action][new_version] = value

    def get_last_version(self, state, action):
        max_version = 0
        for version_string in self.qfunction['0'][state][action].keys():
            version_int = int(version_string)
            if version_int > max_version:
                max_version = version_int
        return str(max_version)

    def squeeze(self, episode):
        for state in self.qfunction['0']:
            for action in self.qfunction['0'][state]:
                version_keys = list(self.qfunction['0'][state][action].keys())
                if episode + 1 < len(version_keys):
                    last_value = self.qfunction['0'][state][action][version_keys[-1]]
                    remove_keys = version_keys[episode:]
                    for k in remove_keys:
                        del self.qfunction['0'][state][action][k]
                    self.qfunction['0'][state][action][str(episode)] = last_value

    def percent_normalize(self):
        for state in self.qfunction['0']:
            actions = self.qfunction['0'][state].keys()
            qfunction_value_list = []
            for action in actions:
                qfunction_value_list.append(self.qfunction['0'][state][action]['0'])

            a = np.array(qfunction_value_list)
            sum = np.sum(a)
            qfunction_value_list = a / sum * 100.0

            for i, action in enumerate(actions):
                self.qfunction['0'][state][action]['0'] = qfunction_value_list[i]

    def percent(self, num_list):
        a = np.array(num_list)
        sum = np.sum(a)
        y = a / sum * 100.0
        return y

    def state_key_exception(self, state):
        if state not in self.qfunction['0']:
            print('state [' + state + '] is not exists on qfunction.')
            raise NameError('state key')

    def key_exception(self, state, action):
        self.state_key_exception(state)
        if action not in self.qfunction['0'][state]:
            print('action [' + action + '] is not exists on qfunction.')
            raise NameError('action key')

    def save_qfunction(self, save_file_name):
        print('\nsave qfunction\t: ' + str(save_file_name) + ' is started.')
        print('size of qfunction(byte) = ', sys.getsizeof(self.qfunction))
        with gzip.GzipFile(filename=save_file_name, mode='w') as save_file:
            save_file.write(json.dumps(obj=self.qfunction).encode(encoding='utf-8'))
        print('save qfunction\t: ' + str(save_file_name) + ' is ended.')

    def print(self):
        print(json.dumps(self.qfunction, indent=4))

if __name__ == '__main__':
    arg = {'qfunction_file' : 'random_qfunction.json'}
    d = QDict(**arg)
    print(d.get_value('0', '1'))
    d.set_value('0', '1', 60.0)
    print(d.get_value('0', '1'))
    # d.saveData('tmp.json')
    tmp = d.get_action_value_dict('0')
    print(type(tmp), tmp)
    # d.print()
