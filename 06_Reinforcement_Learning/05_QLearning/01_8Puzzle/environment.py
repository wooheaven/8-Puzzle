import json
from collections import defaultdict

class Env:
    def __init__(self, reward_file):
        print('reward creation\t\t: from file [' + str(reward_file) + '] is started')
        with open(file=reward_file) as read_file:
            self.reward = json.load(fp=read_file)
        print('reward creation\t\t: from file [' + str(reward_file) + '] is ended')

    def step(self, action):
        next_state = action
        reward = self.reward.get(next_state)
        done = True if (action == '0') else False
        return next_state, reward, done