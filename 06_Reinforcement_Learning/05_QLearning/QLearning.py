import sys
import numpy as np
from environment import Env
from q_dict import QDict

class QLearning:
    def __init__(self, qfunction_file, reward_file):
        self.env = Env(reward_file)
        self.read_qfunction(qfunction_file)
        self.epsilon = 0.9
        self.learning_rate = 0.01
        self.discount_factor = 0.9

    def read_qfunction(self, qfunction_file):
        self.qfunction = QDict(qfunction_file)

    def print_qlearning_head(self):
        print('[------------------------------------', end='')
        print('reward bar--------------------------------->]', end='')
        print(' episode,move,state,action,reward,next_state')

    def qlearning(self, episode, initial_state):
        state = str(initial_state)
        move = 0
        while(True):
            action = self.get_action(state)
            next_state, reward, done = self.env.step(action)
            move += 1
            self.print_status(episode, move, state, action, reward, next_state)
            self.update_qfunction(state, action, reward, next_state)
            state = next_state
            if move == sys.maxsize:
                done = True
            if done:
                break
        self.qfunction.squeeze(episode)

    def get_action(self, state):
        # epsilon greedy policy (implicit policy of qvalue)
        if np.random.rand() < self.epsilon:
            action = self.arg_max(self.qfunction.get_action_value_dict(state))
        else:
            action = np.random.choice(list(self.qfunction.qfunction['0'][state].keys()))
        return action

    def arg_max(self, action_value_dict):
        max_action_list = []
        max_value = -100.0
        for key in action_value_dict.keys():
            value = action_value_dict.get(key)
            if value > max_value:
                max_action_list.clear()
                max_action_list.append(key)
                max_value = value
            elif value == max_value:
                max_action_list.append(key)
        return np.random.choice(max_action_list)

    def update_qfunction(self, state, action, reward, next_state):
        current_qvalue = self.qfunction.get_value(state, action)
        tmp_qvalue = reward + self.discount_factor * max(self.qfunction.get_action_value_dict(next_state).values())
        new_qvalue = current_qvalue + self.learning_rate * (tmp_qvalue - current_qvalue)
        self.qfunction.set_value(state, action, new_qvalue)

    def print_status(self, episode, move, state, action, reward, next_state, bar_length=80):
        arrow = '-' * int(round(reward / 1.0 * bar_length) - 1) + '>'
        spaces = ' ' * (bar_length - len(arrow))
        sys.stdout.write("\r[{0}] {1},{2},{3},{4},{5},{6}"
                         .format(arrow + spaces,
                                 episode, move, state, action, reward, next_state))
        sys.stdout.flush()

    def print_qfunction(self, sample_ids):
        print('QFunction size is ', sys.getsizeof(self.qfunction), '(byte)')
        print('episode,id,value')
        for id in sample_ids:
            print(id, self.qfunction.qfunction['0'][str(id)], sep=',')

    def save_qfunction(self, save_file_name):
        self.qfunction.squeeze(0)
        self.qfunction.percent_normalize()
        self.qfunction.save_qfunction(save_file_name)