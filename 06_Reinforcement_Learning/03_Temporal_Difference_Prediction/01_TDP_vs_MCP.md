# Temporal-Difference Prediction vs Monte Carlo Prediction
```text
In MCP, update valueFunction every episode.
In TDP, update valueFunction every step.
(Episode consists of finite steps.)
```

# ValueFunction의 정의
$` v_{\pi}(s) = E_{\pi}[G_t | S_t = s] `$

# Return을 Recursive하게 정리하기
$` G_i = R_{t+1} + \gamma \cdot R_{i+2} + \gamma^2 \cdot R_{i+3} + ... `$  
$` \kern1em = R_{i+1} + \gamma \cdot ( R_{i+2} + \gamma \cdot R_{i+3} + ... ) `$  
$` \kern1em = R_{i+1} + \gamma \cdot G_{i+1} `$  
Sampleing을 하다보면 근사하게 된다고 가정하면,  
$` \kern1em \approx R_{i+1} + \gamma \cdot G_{i+1} `$ for time step i, i+1, ...

# ValueFunction을 Recursive하게 정리하기
$` v_{\pi}(S_t) = E_{\pi}[R_{t+1} + \gamma \cdot G_{t+1}] `$  
$` v_{\pi}(S_t) \approx E_{\pi}[R_{t+1} + \gamma \cdot v_{\pi}(S_{t+1})] `$ for time step t, t+1, ...  

# 계산 가능한 ValueFunction by MCP
n번째 episode의 (추정된) ValueFunction을 알고 있다고 하자.  
그러면 n+1번째 episode의 (추정된) ValueFunction을 다음과 같이 Update할 수 있다.  
$` v_{\pi, n+1}(S_t) = v_{\pi, n}(S_t) + 1/n \cdot (G_n - v_{\pi, n}(S_t) ) `$

# 계산 가능한 ValueFunction by TDP
누적된 t번째 time step의 (추정된) ValueFunction을 알고 있다고 하자.  
그러면 t번째 time step의 (추정된) ValueFunction을 다음과 같이 Update할 수 있다.  
$` v_{\pi, t}(S_t) = v_{\pi, t}(S_t) + 1/t \cdot ((R_{t+1} + \gamma \cdot v_{\pi}(S_{t+1})) - v_{\pi, t}(S_t) ) `$  
이처럼 다음 상태의 가치함수 예측값을 통해 지금 상태의 가치함수를 예측하는 이러한 방식을 Bootstrap이라고 한다.  
bootstrap의 사전적 뜻은 사물의 초기 단계에서 단순 요소로부터 복잡한 체계를 구축하는 과정이다.  

# MCP vs TDP
In MCP, Update target is $` G_n `$  
In TDP, Update target is $` R_{t+1} + \gamma \cdot v(S_{t+1}) `$  

In MCP, Error is $` G_t - v(S_t)`$  
In TDP, Error is $` (R_{t+1} + \gamma \cdot v(S_{t+1}) - v(S_t) `$  

[MCP vs TDP](06_Reinforcement_Learning/06_Gliffy_Workspace/03_Temporal_Difference_Prediction.png)
