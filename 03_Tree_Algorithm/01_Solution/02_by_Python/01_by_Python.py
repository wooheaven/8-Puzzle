import psycopg2
import pandas as pd


class TreeAlgorithm:
    def __init__(self, conn):
        self.conn = conn

    def get_id_from_state(self, state):
        try:
            cursor = self.conn.cursor()
            query = ""
            query += "SELECT id "
            query += "FROM states "
            query += "WHERE state = '%(state)s' "
            cursor.execute(
                query,
                {'state': psycopg2.extensions.AsIs(state)}
            )
            result = cursor.fetchone()
            return result[0]
        except (Exception, psycopg2.DatabaseError) as error:
            print("Error Happen")
            print(query)
            print(error)
        finally:
            if cursor.closed is False:
                cursor.close()

    def create_root_child_graph(self, conn, next_id):
        try:
            cursor = self.conn.cursor()

            # drop
            drop_query = ""
            drop_query += "DROP TABLE "
            drop_query += "if EXISTS root_child_graph "
            cursor.execute(drop_query)
            conn.commit()

            # create
            create_query = ""
            create_query += "CREATE TABLE root_child_graph AS (                                   " + "\n"
            create_query += "  SELECT                                                           " + "\n"
            create_query += "    id,                                                          " + "\n"
            create_query += "    next_id                                                      " + "\n"
            create_query += "  FROM (                                                           " + "\n"
            create_query += "    SELECT                                                       " + "\n"
            create_query += "      id,                                                      " + "\n"
            create_query += "      unnest(string_to_array(next_ids, ',')) :: INT AS next_id " + "\n"
            create_query += "    FROM states                                                  " + "\n"
            create_query += "    WHERE id <= %(next_id)s                                      " + "\n"
            create_query += "    ORDER BY id                                                  " + "\n"
            create_query += "  ) AS a                                                           " + "\n"
            create_query += "  WHERE                                                            " + "\n"
            create_query += "    id < next_id -- pick up as tree                              " + "\n"
            create_query += "    AND next_id <= %(next_id)s                                   " + "\n"
            create_query += "  ORDER BY id , next_id                                            " + "\n"
            create_query += ") "
            create_query = cursor.mogrify(
                create_query,
                {'next_id': psycopg2.extensions.AsIs(next_id)}
            )
            cursor.execute(
                create_query
            )
            conn.commit()
        except (Exception, psycopg2.DatabaseError) as error:
            print("Error Happen")
            print(str(create_query))
            print(error)
        finally:
            if cursor.closed is False:
                cursor.close()

    def get_shortest_path(self, conn, next_id):
        try:
            cursor = self.conn.cursor()

            # select
            select_query = ""
            select_query += "WITH recursive search (            " + "\n"
            select_query += "  id, next_id,                     " + "\n"
            select_query += "  length, id_path, cycle) AS (     " + "\n"
            select_query += "  -- root node                     " + "\n"
            select_query += "  SELECT                           " + "\n"
            select_query += "    g.id, g.next_id, 1,            " + "\n"
            select_query += "    ARRAY[g.id, g.next_id], FALSE  " + "\n"
            select_query += "  FROM root_child_graph AS g       " + "\n"
            select_query += "  WHERE g.id = 0                   " + "\n"
            select_query += "  -- root + child node             " + "\n"
            select_query += "  UNION ALL                        " + "\n"
            select_query += "  -- child node to leaf node       " + "\n"
            select_query += "  SELECT                           " + "\n"
            select_query += "    g.id, g.next_id, s.length + 1, " + "\n"
            select_query += "    id_path || g.next_id,          " + "\n"
            select_query += "    g.next_id = ANY(id_path)       " + "\n"
            select_query += "  FROM                             " + "\n"
            select_query += "    root_child_graph AS g,         " + "\n"
            select_query += "    search AS s                    " + "\n"
            select_query += "  WHERE g.id = s.next_id           " + "\n"
            select_query += "    AND g.id < g.next_id           " + "\n"
            select_query += "    AND g.next_id <= %(next_id)s   " + "\n"
            select_query += "    AND NOT cycle                  " + "\n"
            select_query += ")                                  " + "\n"
            select_query += "SELECT                             " + "\n"
            select_query += "  id, next_id, length, id_path     " + "\n"
            select_query += "FROM search                        " + "\n"
            select_query += "WHERE next_id in (                 " + "\n"
            select_query += "  SELECT unnest(id_path)           " + "\n"
            select_query += "  FROM search                      " + "\n"
            select_query += "  WHERE next_id = %(next_id)s      " + "\n"
            select_query += ")                                  " + "\n"
            select_query += "ORDER BY                           " + "\n"
            select_query += "  id_path, id, next_id             "
            select_query = cursor.mogrify(
                select_query,
                {'next_id': psycopg2.extensions.AsIs(next_id)}
            )
            # cursor.execute(select_query)
            # result = cursor.fetchall()
            # return result
            return pd.read_sql(sql=select_query, con=conn)
        except (Exception, psycopg2.DatabaseError) as error:
            print("Error Happen")
            print(select_query)
            print(error)
        finally:
            if cursor.closed is False:
                cursor.close()


if __name__ == "__main__":
    conn = psycopg2.connect("dbname='mydatabase' user='myuser' host='localhost' port='65432' password='123qwe'")
    sol = TreeAlgorithm(conn)

    # state
    # 8 3 0
    # 4 1 6
    # 2 7 5
    state = '830416275'
    print('state\t= ', state)

    # id
    id = sol.get_id_from_state(state)
    print('id\t\t= ', id)

    # root_child_graph
    sol.create_root_child_graph(conn, id)

    # shortest_path
    shortest_path = sol.get_shortest_path(conn, id)
    # print(shortest_path)  # cursor.fetchall()
    with pd.option_context('display.max_colwidth', 150,
                           'display.max_columns', 5,
                           'display.width', 400):
        print(shortest_path)

    conn.close()
