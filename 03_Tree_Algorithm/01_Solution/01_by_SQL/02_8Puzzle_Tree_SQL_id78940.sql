-- find length and shortest path for [id 78940]
-- state
-- 8 3 0
-- 4 1 6
-- 2 7 5

mydatabase=> DROP TABLE if EXISTS root_child_graph;
DROP TABLE

mydatabase=>
CREATE TABLE root_child_graph AS (
    SELECT
        id,
        next_id
    FROM (
        SELECT
            id,
            unnest(string_to_array(next_ids, ',')) :: INT AS next_id
        FROM states
        WHERE id <= 78940
    ) AS a
    WHERE
        id < next_id -- pick up as spanning tree
        AND next_id <= 78940
    ORDER BY id, next_id
);
SELECT 93086

mydatabase=>
WITH recursive search (id, next_id, length, id_path, cycle) AS (
        -- root node
        SELECT g.id, g.next_id, 1, ARRAY[g.id, g.next_id], FALSE
        FROM root_child_graph AS g
        WHERE g.id = 0
    UNION ALL
        -- child node to leaf node
        SELECT g.id, g.next_id, s.length + 1, id_path || g.next_id, g.next_id = ANY(id_path)
        FROM root_child_graph AS g, search AS s
        WHERE g.id = s.next_id AND g.id < g.next_id AND g.next_id <= 78940 AND NOT cycle
)
SELECT id, next_id, length, id_path
FROM search
WHERE next_id in ( SELECT unnest(id_path) FROM search WHERE next_id = 78940 )
ORDER BY id_path, id, next_id
;
  id   | next_id | length |                                               id_path                                                
-------+---------+--------+------------------------------------------------------------------------------------------------------
     0 |       1 |      1 | {0,1}
     1 |       4 |      2 | {0,1,4}
     4 |       8 |      3 | {0,1,4,8}
     8 |      17 |      4 | {0,1,4,8,17}
    17 |      35 |      5 | {0,1,4,8,17,35}
    35 |      59 |      6 | {0,1,4,8,17,35,59}
    59 |     102 |      7 | {0,1,4,8,17,35,59,102}
   102 |     176 |      8 | {0,1,4,8,17,35,59,102,176}
   176 |     301 |      9 | {0,1,4,8,17,35,59,102,176,301}
   301 |     481 |     10 | {0,1,4,8,17,35,59,102,176,301,481}
   481 |     792 |     11 | {0,1,4,8,17,35,59,102,176,301,481,792}
   792 |    1264 |     12 | {0,1,4,8,17,35,59,102,176,301,481,792,1264}
  1264 |    2072 |     13 | {0,1,4,8,17,35,59,102,176,301,481,792,1264,2072}
  2072 |    3281 |     14 | {0,1,4,8,17,35,59,102,176,301,481,792,1264,2072,3281}
  3281 |    5324 |     15 | {0,1,4,8,17,35,59,102,176,301,481,792,1264,2072,3281,5324}
  5324 |    8303 |     16 | {0,1,4,8,17,35,59,102,176,301,481,792,1264,2072,3281,5324,8303}
  8303 |   13085 |     17 | {0,1,4,8,17,35,59,102,176,301,481,792,1264,2072,3281,5324,8303,13085}
 13085 |   19721 |     18 | {0,1,4,8,17,35,59,102,176,301,481,792,1264,2072,3281,5324,8303,13085,19721}
 19721 |   29634 |     19 | {0,1,4,8,17,35,59,102,176,301,481,792,1264,2072,3281,5324,8303,13085,19721,29634}
 29634 |   42287 |     20 | {0,1,4,8,17,35,59,102,176,301,481,792,1264,2072,3281,5324,8303,13085,19721,29634,42287}
 42287 |   59548 |     21 | {0,1,4,8,17,35,59,102,176,301,481,792,1264,2072,3281,5324,8303,13085,19721,29634,42287,59548}
 59548 |   78940 |     22 | {0,1,4,8,17,35,59,102,176,301,481,792,1264,2072,3281,5324,8303,13085,19721,29634,42287,59548,78940}
     0 |       2 |      1 | {0,2}
     2 |       5 |      2 | {0,2,5}
     5 |      11 |      3 | {0,2,5,11}
    11 |      23 |      4 | {0,2,5,11,23}
    23 |      41 |      5 | {0,2,5,11,23,41}
    41 |      71 |      6 | {0,2,5,11,23,41,71}
    71 |     122 |      7 | {0,2,5,11,23,41,71,122}
   122 |     214 |      8 | {0,2,5,11,23,41,71,122,214}
   214 |     346 |      9 | {0,2,5,11,23,41,71,122,214,346}
   346 |     568 |     10 | {0,2,5,11,23,41,71,122,214,346,568}
   568 |     913 |     11 | {0,2,5,11,23,41,71,122,214,346,568,913}
   913 |    1493 |     12 | {0,2,5,11,23,41,71,122,214,346,568,913,1493}
  1493 |    2391 |     13 | {0,2,5,11,23,41,71,122,214,346,568,913,1493,2391}
  2391 |    3872 |     14 | {0,2,5,11,23,41,71,122,214,346,568,913,1493,2391,3872}
  3872 |    6101 |     15 | {0,2,5,11,23,41,71,122,214,346,568,913,1493,2391,3872,6101}
  6101 |    9691 |     16 | {0,2,5,11,23,41,71,122,214,346,568,913,1493,2391,3872,6101,9691}
  9691 |   14808 |     17 | {0,2,5,11,23,41,71,122,214,346,568,913,1493,2391,3872,6101,9691,14808}
 14808 |   22698 |     18 | {0,2,5,11,23,41,71,122,214,346,568,913,1493,2391,3872,6101,9691,14808,22698}
 22698 |   32987 |     19 | {0,2,5,11,23,41,71,122,214,346,568,913,1493,2391,3872,6101,9691,14808,22698,32987}
 32987 |   47581 |     20 | {0,2,5,11,23,41,71,122,214,346,568,913,1493,2391,3872,6101,9691,14808,22698,32987,47581}
 47581 |   59548 |     21 | {0,2,5,11,23,41,71,122,214,346,568,913,1493,2391,3872,6101,9691,14808,22698,32987,47581,59548}
 59548 |   78940 |     22 | {0,2,5,11,23,41,71,122,214,346,568,913,1493,2391,3872,6101,9691,14808,22698,32987,47581,59548,78940}
   913 |    1494 |     12 | {0,2,5,11,23,41,71,122,214,346,568,913,1494}
  1494 |    2392 |     13 | {0,2,5,11,23,41,71,122,214,346,568,913,1494,2392}
  2392 |    3875 |     14 | {0,2,5,11,23,41,71,122,214,346,568,913,1494,2392,3875}
  3875 |    6104 |     15 | {0,2,5,11,23,41,71,122,214,346,568,913,1494,2392,3875,6104}
  6104 |    9696 |     16 | {0,2,5,11,23,41,71,122,214,346,568,913,1494,2392,3875,6104,9696}
  9696 |   14815 |     17 | {0,2,5,11,23,41,71,122,214,346,568,913,1494,2392,3875,6104,9696,14815}
 14815 |   22712 |     18 | {0,2,5,11,23,41,71,122,214,346,568,913,1494,2392,3875,6104,9696,14815,22712}
 22712 |   32987 |     19 | {0,2,5,11,23,41,71,122,214,346,568,913,1494,2392,3875,6104,9696,14815,22712,32987}
 32987 |   47581 |     20 | {0,2,5,11,23,41,71,122,214,346,568,913,1494,2392,3875,6104,9696,14815,22712,32987,47581}
 47581 |   59548 |     21 | {0,2,5,11,23,41,71,122,214,346,568,913,1494,2392,3875,6104,9696,14815,22712,32987,47581,59548}
 59548 |   78940 |     22 | {0,2,5,11,23,41,71,122,214,346,568,913,1494,2392,3875,6104,9696,14815,22712,32987,47581,59548,78940}
(55 rows)
