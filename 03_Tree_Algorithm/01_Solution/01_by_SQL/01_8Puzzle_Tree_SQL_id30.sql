--find length and shortest path for [id 30]
--id 30
--state
--1 2 3
--5 0 6
--4 7 8

mydatabase=> DROP TABLE if EXISTS root_child_graph;

mydatabase=> 
CREATE TABLE root_child_graph AS (
    SELECT 
        id,
        next_id
    FROM (
        SELECT
            id,
            unnest(string_to_array(next_ids, ',')) :: INT AS next_id
        FROM states
        WHERE id <= 30
    ) AS a
    WHERE
        id < next_id -- pick up as spanning tree
        AND next_id <= 30
    ORDER BY id, next_id
);
SELECT 30

mydatabase=> SELECT * FROM root_child_graph ;
 id | next_id 
----+---------
  0 |       1
  0 |       2
  1 |       3
  1 |       4
  2 |       5
  2 |       6
  3 |       7
  4 |       8
  4 |       9
  4 |      10
  5 |      11
  5 |      12
  5 |      13
  6 |      14
  7 |      15
  7 |      16
  8 |      17
  8 |      18
  9 |      19
  9 |      20
 10 |      21
 10 |      22
 11 |      23
 11 |      24
 12 |      25
 12 |      26
 13 |      27
 13 |      28
 14 |      29
 14 |      30
(30 rows)

mydatabase=> 
WITH recursive search (id, next_id, length, id_path, cycle) AS (
        -- root node
        SELECT g.id, g.next_id, 1, ARRAY[g.id, g.next_id], FALSE
        FROM root_child_graph AS g
        WHERE g.id = 0
    UNION ALL
        -- child node to leaf node
        SELECT g.id, g.next_id, s.length + 1, id_path || g.next_id, g.next_id = ANY(id_path)
        FROM root_child_graph AS g, search AS s
        WHERE g.id = s.next_id AND g.id < g.next_id AND g.next_id <= 30 AND NOT cycle
)
SELECT id, next_id, length, id_path
FROM search
WHERE next_id in ( SELECT unnest(id_path) FROM search WHERE next_id = 30 )
ORDER BY id_path, id, next_id;
 id | next_id | length |    id_path    
----+---------+--------+---------------
  0 |       2 |      1 | {0,2}
  2 |       6 |      2 | {0,2,6}
  6 |      14 |      3 | {0,2,6,14}
 14 |      30 |      4 | {0,2,6,14,30}
(4 rows)

-- Exploratory Data Analysis
mydatabase=> 
SELECT id,next_ids,state 
FROM states 
WHERE id <= 30 ;
 id |  next_ids   |   state   
----+-------------+-----------
  0 | 1,2         | 123456780
  1 | 3,4,0       | 123450786
  2 | 5,6,0       | 123456708
  3 | 7,1         | 120453786
  4 | 8,9,1,10    | 123405786
  5 | 11,12,13,2  | 123406758
  6 | 14,2        | 123456078
  7 | 15,3,16     | 102453786
  8 | 17,18,4     | 103425786
  9 | 19,4,20     | 123045786
 10 | 4,21,22     | 123485706
 11 | 23,24,5     | 103426758
 12 | 25,5,26     | 123046758
 13 | 27,5,28     | 123460758
 14 | 29,30,6     | 123056478
 15 | 7,31        | 012453786
 16 | 7,32,33,34  | 152403786
 17 | 8,35        | 013425786
 18 | 8,36        | 130425786
 19 | 37,9        | 023145786
 20 | 9,38        | 123745086
 21 | 39,10       | 123485076
 22 | 40,10       | 123485760
 23 | 11,41       | 013426758
 24 | 11,42       | 130426758
 25 | 43,12       | 023146758
 26 | 12,44       | 123746058
 27 | 45,13       | 120463758
 28 | 13,46       | 123468750
 29 | 47,14       | 023156478
 30 | 48,14,49,50 | 123506478
(31 rows)

mydatabase=> 
SELECT id,unnest(string_to_array(next_ids, ',')) :: INT AS next_id 
FROM states 
WHERE id <= 30;
 id | next_id 
----+---------
  0 |       1
  0 |       2
  1 |       3
  1 |       4
  1 |       0
  2 |       5
  2 |       6
  2 |       0
  3 |       7
  3 |       1
  4 |       8
  4 |       9
  4 |       1
  4 |      10
  5 |      11
  5 |      12
  5 |      13
  5 |       2
  6 |      14
  6 |       2
  7 |      15
  7 |       3
  7 |      16
  8 |      17
  8 |      18
  8 |       4
  9 |      19
  9 |       4
  9 |      20
 10 |       4
 10 |      21
 10 |      22
 11 |      23
 11 |      24
 11 |       5
 12 |      25
 12 |       5
 12 |      26
 13 |      27
 13 |       5
 13 |      28
 14 |      29
 14 |      30
 14 |       6
 15 |       7
 15 |      31
 16 |       7
 16 |      32
 16 |      33
 16 |      34
 17 |       8
 17 |      35
 18 |       8
 18 |      36
 19 |      37
 19 |       9
 20 |       9
 20 |      38
 21 |      39
 21 |      10
 22 |      40
 22 |      10
 23 |      11
 23 |      41
 24 |      11
 24 |      42
 25 |      43
 25 |      12
 26 |      12
 26 |      44
 27 |      45
 27 |      13
 28 |      13
 28 |      46
 29 |      47
 29 |      14
 30 |      48
 30 |      14
 30 |      49
 30 |      50
(80 rows)

mydatabase=> 
SELECT id,next_id 
FROM ( 
  SELECT id,unnest(string_to_array(next_ids, ',')) :: INT AS next_id 
  FROM states 
  WHERE id <= 30 
  ORDER BY id
) AS a 
WHERE id < next_id AND next_id <= 30 
ORDER BY id, next_id ;

 id | next_id 
----+---------
  0 |       1
  0 |       2
  1 |       3
  1 |       4
  2 |       5
  2 |       6
  3 |       7
  4 |       8
  4 |       9
  4 |      10
  5 |      11
  5 |      12
  5 |      13
  6 |      14
  7 |      15
  7 |      16
  8 |      17
  8 |      18
  9 |      19
  9 |      20
 10 |      21
 10 |      22
 11 |      23
 11 |      24
 12 |      25
 12 |      26
 13 |      27
 13 |      28
 14 |      29
 14 |      30
(30 rows)

mydatabase=> 
WITH recursive search (id, next_id, length, id_path, cycle) AS (
        -- root node
        SELECT g.id, g.next_id, 1, ARRAY[g.id, g.next_id], FALSE
        FROM root_child_graph AS g
        WHERE g.id = 0
    UNION ALL
        -- child node to leaf node
        SELECT g.id, g.next_id, s.length + 1, id_path || g.next_id, g.next_id = ANY(id_path)
        FROM root_child_graph AS g, search AS s
        WHERE g.id = s.next_id AND g.id < g.next_id AND g.next_id <= 30 AND NOT cycle
)
SELECT id, next_id, length, id_path
FROM search
ORDER BY length, id, next_id;
 id | next_id | length |    id_path    
----+---------+--------+---------------
  0 |       1 |      1 | {0,1}
  0 |       2 |      1 | {0,2}
  1 |       3 |      2 | {0,1,3}
  1 |       4 |      2 | {0,1,4}
  2 |       5 |      2 | {0,2,5}
  2 |       6 |      2 | {0,2,6}
  3 |       7 |      3 | {0,1,3,7}
  4 |       8 |      3 | {0,1,4,8}
  4 |       9 |      3 | {0,1,4,9}
  4 |      10 |      3 | {0,1,4,10}
  5 |      11 |      3 | {0,2,5,11}
  5 |      12 |      3 | {0,2,5,12}
  5 |      13 |      3 | {0,2,5,13}
  6 |      14 |      3 | {0,2,6,14}
  7 |      15 |      4 | {0,1,3,7,15}
  7 |      16 |      4 | {0,1,3,7,16}
  8 |      17 |      4 | {0,1,4,8,17}
  8 |      18 |      4 | {0,1,4,8,18}
  9 |      19 |      4 | {0,1,4,9,19}
  9 |      20 |      4 | {0,1,4,9,20}
 10 |      21 |      4 | {0,1,4,10,21}
 10 |      22 |      4 | {0,1,4,10,22}
 11 |      23 |      4 | {0,2,5,11,23}
 11 |      24 |      4 | {0,2,5,11,24}
 12 |      25 |      4 | {0,2,5,12,25}
 12 |      26 |      4 | {0,2,5,12,26}
 13 |      27 |      4 | {0,2,5,13,27}
 13 |      28 |      4 | {0,2,5,13,28}
 14 |      29 |      4 | {0,2,6,14,29}
 14 |      30 |      4 | {0,2,6,14,30}
(30 rows)
