# 1. Must Perfect information of Environment
reward $`\approx`$ manhattan distance  
State Transition Probability $`\approx`$ 1  

# 2. Curse of Dimention
```{text}
8-Puzzle's state is made of 3*3 matrix(2D) template.
Cube's state is made of 3*3*6(3D) template.
```
[Cube Link](https://rubiks-cube-solver.com)

# 3. Computational complexity
$`\approx \{ size(states) \} ^3`$  

# Weakness of Dynamic Programming Flow
```mermaid
graph TD
A[Dynamic Programming] -->B(Check Environment Condition Start)
B --> C{Reward is given ?}
C -->|Yes| D{State Transition Probability is given ?}
C -->|No| E(Imposible)
D -->|Yes| F{States is findable ?}
D -->|No| E
F -->|Yes|G{States is countable ?}
F -->|No| E
G --> |Yes| H(Check Environment Condition End)
G -->|No| E
```
