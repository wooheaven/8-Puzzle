# Policy Iteration
```
explicit policy = Policy Evaluation (= ValueFunction Update by BEE)
                + Policy Improvement
```

# Value Iteration
```
implicit policy = Optimal ValueFunction (= ValueFunction Update by BOE)
                ≈ BEE + Greedy Policy
```