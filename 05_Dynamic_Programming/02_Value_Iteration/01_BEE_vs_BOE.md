# 계산 가능한 Bellman Expectation Equation(BEE)  
$`v_{\pi, k+1}(S_t) = \displaystyle\sum_{a \in A} {\pi}(a|S_t) \cdot \{ R_{t+1} + \gamma \cdot v_{\pi, k}(S_{t+1}) \}`$  

# 계산 가능한 Bellman Optimal Equation(BOE)  
$`v_{k+1}(S_t) = \displaystyle\max_{a \in A} \{ R_{t+1} + \gamma \cdot v_{k}(S_{t+1}) \}`$  

# 계산 과정 Iteration
```
1) Suppose that valueFunction is for current(or optimal) policy
2) calculate by iteration
3) valueFunction becomes true valueFunction of current(or optimal) policy
```