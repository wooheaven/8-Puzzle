# BEE with iteration

$` v_{\pi, k+1}(S_t) = \displaystyle\sum_{a \in A} {\pi}(a|S_t) \cdot \{ R_{t+1} + \gamma \cdot v_{\pi, k}(S_{t+1}) \}`$  

# new with iteration

$` v_{\pi, k+1}(S_t) = [ v_{\pi, k}(S_t) + \displaystyle\sum_{a \in A} {\pi}(a|S_t) \cdot \{ R_{t+1} + \gamma \cdot v_{\pi, k}(S_{t+1}) \} ]`$  
