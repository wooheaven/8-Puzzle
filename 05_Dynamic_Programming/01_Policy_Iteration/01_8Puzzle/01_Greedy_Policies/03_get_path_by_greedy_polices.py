import psycopg2

class path_by_greedy_policies:
    def __init__(self, conn):
        self.conn = conn
        self.incomplete = True

    def get(self, iteration, id):
        self.incomplete = True
        try:
            cursor = self.conn.cursor()
            query = ""
            query += "WITH recursive search                                                                         " + "\n"
            query += "    (policy_id, iteration, id, next_id, proability,                                           " + "\n"
            query += "        length, proability_path, id_path, cycle) AS (                                         " + "\n"
            query += "    -- leaf node                                                                              " + "\n"
            query += "    SELECT                                                                                    " + "\n"
            query += "        g.policy_id, g.iteration, g.id, g.next_id, g.proability,                              " + "\n"
            query += "        1 :: int AS length,                                                                   " + "\n"
            query += "        ARRAY[g.proability] AS proability_path,                                               " + "\n"
            query += "        ARRAY[g.id, g.next_id] AS id_path,                                                    " + "\n"
            query += "        FALSE AS cycle                                                                        " + "\n"
            query += "    FROM (                                                                                    " + "\n"
            query += "        SELECT policy_id, iteration, id, next_id, proability                                  " + "\n"
            query += "        FROM (                                                                                " + "\n"
            query += "            SELECT *, rank() OVER (ORDER BY proability DESC, next_id) AS rank                 " + "\n"
            query += "            FROM greedy_graph                                                                 " + "\n"
            query += "            WHERE iteration = %s AND id = %s                                                  " + "\n"
            query += "        ) AS a                                                                                " + "\n"
            query += "        WHERE a.rank = 1                                                                      " + "\n"
            query += "    ) AS g                                                                                    " + "\n"
            query += "UNION ALL                                                                                     " + "\n"
            query += "    -- parent node to root node                                                               " + "\n"
            query += "    SELECT                                                                                    " + "\n"
            query += "        policy_id, iteration, id, next_id, proability,                                        " + "\n"
            query += "        length,                                                                               " + "\n"
            query += "        proability_path, id_path, cycle                                                       " + "\n"
            query += "    FROM (                                                                                    " + "\n"
            query += "        SELECT                                                                                " + "\n"
            query += "            g.policy_id, g.iteration, g.id, g.next_id, g.proability,                          " + "\n"
            query += "            rank() OVER (ORDER BY g.proability DESC, g.next_id) AS rank,                      " + "\n"
            query += "            s.length + 1 as length,                                                           " + "\n"
            query += "            ARRAY_APPEND(s.proability_path, g.proability)::numeric(5,2)[] AS proability_path, " + "\n"
            query += "            s.id_path || g.next_id as id_path,                                                " + "\n"
            query += "            g.next_id = ANY(s.id_path) AS cycle                                               " + "\n"
            query += "        FROM greedy_graph AS g, search AS s                                                   " + "\n"
            query += "        WHERE g.iteration = s.iteration AND g.id = s.next_id AND g.id > g.next_id             " + "\n"
            query += "    ) AS a                                                                                    " + "\n"
            query += "    WHERE a.rank = 1 AND NOT a.cycle                                                          " + "\n"
            query += ")                                                                                             " + "\n"
            query += "SELECT                                                                                        " + "\n"
            query += "    policy_id, iteration, id, next_id, proability,                                            " + "\n"
            query += "    length, proability_path, id_path                                                          " + "\n"
            query += "FROM search                                                                                   "
            cursor.execute(query, (iteration, id))
            with open(file='04_greedy_policies.log', mode='a') as append_file:
                append_file.write('\n')
                for row in cursor.fetchall():
                    print(row[0], row[1], row[2], row[3], row[4], row[5], '', sep='\t', end='')
                    tmp_str = '\t'.join(str(row[i]) for i in range(6))
                    append_file.write(tmp_str)
                    proability_path_str = '[' + ', '.join(str(e) for e in row[6]) + ']'
                    print(proability_path_str, row[7], sep='\t')
                    append_file.write('\t' + proability_path_str + '\t' + str(row[7]) + '\n')
                    if row[7][-1] == 0:
                        self.incomplete = False
            print('')
        except (Exception, psycopg2.DatabaseError) as error:
            print("Error Happen")
            print(query)
            print(error)
        finally:
            if cursor.closed is False:
                cursor.close()

if __name__ == "__main__":
    conn = psycopg2.connect("dbname='mydatabase' user='myuser' host='localhost' port='65432' password='123qwe'")
    g = path_by_greedy_policies(conn)
    iteration = 1
    id = 1
    while iteration < 51 and id < 181440:
        g.get(iteration, id)
        if g.incomplete:
            iteration += 1
        else:
            id += 1
    conn.close()
