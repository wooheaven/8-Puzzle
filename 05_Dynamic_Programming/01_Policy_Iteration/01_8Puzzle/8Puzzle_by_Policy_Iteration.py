from policy_iteration import Policy_Iteration

if __name__ == '__main__':
    arg = {'policy_file' : 'random_policy.json',
           'reward_file' : 'reward_by_manhattan_distance.json'}
    learning = Policy_Iteration(**arg)

    # Create random policy from adjacency
    # arg = {'read_file_name' : 'adjacency_dict.json',
    #        'save_file_name' : 'random_policy.json'}
    # learning.save_random_policy_from_adjacency(**arg)

    iteration_size = 51
    # run_mode = 'g'
    run_mode = 's'
    # run_mode = 'a'
    learning.policy_iteration(iteration_size, mode=run_mode)

    # sample_iteration_size = 51
    # sample_iterations = range(min(sample_iteration_size, iteration_size))
    # sample_id_size = 3
    # sample_ids = range(sample_id_size)
    # learning.print_value_functions(sample_iterations, sample_ids)
    # learning.print_policies(sample_iterations, sample_ids)

    # file_name = '01_Greedy_Policies/01_greedy_policies_50iterations.json.gz'
    # file_name = '02_Softmax_Policies/01_softmax_policies_50iterations.json.gz'
    # file_name = '03_Asis_Policies/01_asis_policies_50iterations.json.gz'
    file_name = '04_New_Policy_Evaluation/01_new_policies_50iterations.json.gz'
    learning.save_policies(policies_file_name=file_name, mode='gz')
    # learning.load_policies(file_name, mode='gz')
    # learning.save_policies('01_Greedy_Policies/01_greedy_policies_50iterations.csv', mode='csv')
    # learning.save_policies('02_Softmax_Policies/01_softmax_policies_50iterations.csv', mode='csv')
    # learning.save_policies('03_Asis_Policies/01_asis_policies_50iterations.csv', mode='csv')
    learning.save_policies('04_New_Policy_Evaluation/01_new_policies_50iterations.csv', mode='csv')
