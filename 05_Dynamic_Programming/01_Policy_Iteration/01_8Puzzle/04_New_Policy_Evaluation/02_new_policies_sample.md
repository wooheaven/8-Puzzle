# copy new_policies data
```{bash}
$ head 01_new_policies_50iterations.csv
0,0,1,50.0
0,0,2,50.0
0,1,0,33.33
0,1,3,33.33
0,1,4,33.33
0,2,0,33.33
0,2,5,33.33
0,2,6,33.33
0,3,1,50.0
0,3,7,50.0

$ docker cp 01_new_policies_50iterations.csv pg:/01_new_policies_50iterations.csv

$ docker exec -it pg bash

root@ae6b9915c8d5:/# ./mydatabase.sh
psql (10.2 (Debian 10.2-1.pgdg90+1))
Type "help" for help.

```

# insert new_policies data
```{sql}
mydatabase=> DROP TABLE IF EXISTS new_graph ;
DROP TABLE

mydatabase=>
CREATE TABLE new_graph (
    policy_id  serial primary key,
    iteration  bigint,
    id         bigint,
    next_id    bigint,
    proability numeric(5,2) ) ;
CREATE TABLE

mydatabase=> \d new_graph_policy_id_seq
              Sequence "public.new_graph_policy_id_seq"
  Type   | Start | Minimum |  Maximum   | Increment | Cycles? | Cache 
---------+-------+---------+------------+-----------+---------+-------
 integer |     1 |       1 | 2147483647 |         1 | no      |     1
Owned by: public.new_graph.policy_id

mydatabase=> ALTER SEQUENCE new_graph_policy_id_seq minvalue 0 start 0 restart with 0;
ALTER SEQUENCE

mydatabase=> \d new_graph_policy_id_seq
              Sequence "public.new_graph_policy_id_seq"
  Type   | Start | Minimum |  Maximum   | Increment | Cycles? | Cache 
---------+-------+---------+------------+-----------+---------+-------
 integer |     0 |       0 | 2147483647 |         1 | no      |     1
Owned by: public.new_graph.policy_id

mydatabase=> SELECT nextval('new_graph_policy_id_seq') ;
 nextval 
---------
       0
(1 row)

mydatabase=> \COPY new_graph(iteration,id,next_id,proability) FROM '/01_new_policies_50iterations.csv' DELIMITER ',' ;
COPY 24675840

mydatabase=> SELECT nextval('new_graph_policy_id_seq') ;
 nextval  
----------
 24675841
(1 row)
```

# search new_policies
```{sql}
mydatabase=> 
SELECT *
FROM new_graph
WHERE iteration = 2 AND id = 30 ;
 policy_id | iteration | id | next_id | proability 
-----------+-----------+----+---------+------------
    967757 |         2 | 30 |      14 |      99.99
    967758 |         2 | 30 |      48 |       0.00
    967759 |         2 | 30 |      49 |       0.00
    967760 |         2 | 30 |      50 |       0.01
(4 rows)

mydatabase=> 
SELECT *,rank() OVER (ORDER BY proability DESC, next_id) AS rank
FROM new_graph
WHERE iteration = 2 AND id = 30 ;
 policy_id | iteration | id | next_id | proability | rank
-----------+-----------+----+---------+------------+------
    967757 |         2 | 30 |      14 |      99.99 |    1
    967760 |         2 | 30 |      50 |       0.01 |    2
    967758 |         2 | 30 |      48 |       0.00 |    3
    967759 |         2 | 30 |      49 |       0.00 |    4
(4 rows)

mydatabase=> 
SELECT * 
FROM (
    SELECT *,rank() OVER (ORDER BY proability DESC, next_id) AS rank 
    FROM new_graph 
    WHERE iteration = 2 AND id = 30 
) AS a
WHERE a.rank = 1 ;
 policy_id | iteration | id | next_id | proability | rank 
-----------+-----------+----+---------+------------+------
    967757 |         2 | 30 |      14 |      99.99 |    1
(1 row)

mydatabase=>
SELECT 
    g.policy_id, g.iteration, g.id, g.next_id, g.proability,
    1 :: int AS length, 
    ARRAY[g.proability] AS proability_path, 
    ARRAY[g.id, g.next_id] AS id_path, 
    FALSE AS cycle
FROM (
    SELECT policy_id, iteration, id, next_id, proability 
    FROM ( 
        SELECT *, rank() OVER (ORDER BY proability DESC, next_id) AS rank 
        FROM new_graph 
        WHERE iteration = 2 AND id = 30 
    ) AS a 
    WHERE 
        a.rank = 1
) AS g ;
 policy_id | iteration | id | next_id | proability | length | proability_path | id_path | cycle
-----------+-----------+----+---------+------------+--------+-----------------+---------+-------
    967757 |         2 | 30 |      14 |      99.99 |      1 | {99.99}         | {30,14} | f
(1 row)

mydatabase=>
SELECT 
    g.policy_id, g.iteration, g.id, g.next_id, g.proability,
    1 :: int AS length, 
    ARRAY[g.proability] AS proability_path, 
    ARRAY[g.id, g.next_id] AS id_path, 
    FALSE AS cycle
FROM (
    SELECT policy_id, iteration, id, next_id, proability 
    FROM ( 
        SELECT *, rank() OVER (ORDER BY proability DESC, next_id) AS rank 
        FROM new_graph 
        WHERE iteration = 2 AND id = 14 
    ) AS a 
    WHERE 
        a.rank = 1
) AS g ;
 policy_id | iteration | id | next_id | proability | length | proability_path | id_path | cycle
-----------+-----------+----+---------+------------+--------+-----------------+---------+-------
    967722 |         2 | 14 |       6 |     100.00 |      1 | {100.00}        | {14,6}  | f
(1 row)

mydatabase=>
WITH recursive search (policy_id, iteration, id, next_id, proability, length, proability_path, id_path, cycle) AS (
        -- root node
        SELECT 
            g.policy_id, g.iteration, g.id, g.next_id, g.proability,
            1 :: int AS length, 
            ARRAY[g.proability] AS proability_path, 
            ARRAY[g.id, g.next_id] AS id_path, 
            FALSE AS cycle
        FROM (
            SELECT policy_id, iteration, id, next_id, proability 
            FROM ( 
                SELECT *, rank() OVER (ORDER BY proability DESC, next_id) AS rank 
                FROM new_graph 
                WHERE iteration = 2 AND id = 30 
            ) AS a 
            WHERE 
                a.rank = 1
        ) AS g
    UNION ALL
        -- child node to leaf node
        SELECT 
            policy_id, iteration, id, next_id, proability,
            length, 
            proability_path, 
            id_path, 
            cycle
        FROM (
            SELECT 
                g.policy_id, g.iteration, g.id, g.next_id, g.proability, 
                rank() OVER (ORDER BY g.proability DESC, g.next_id) AS rank,
                s.length + 1 as length,
                ARRAY_APPEND(s.proability_path, g.proability) :: numeric(5,2)[] AS proability_path, 
                s.id_path || g.next_id as id_path, 
                g.next_id = ANY(s.id_path) as cycle
            FROM new_graph AS g, search AS s
            WHERE g.iteration = s.iteration AND g.id = s.next_id AND g.id > g.next_id
        ) AS a
        WHERE a.rank = 1 AND NOT a.cycle 
)
SELECT policy_id, iteration, id, next_id, proability, length, proability_path, id_path FROM search ;
 policy_id | iteration | id | next_id | proability | length |      proability_path      |    id_path    
-----------+-----------+----+---------+------------+--------+---------------------------+---------------
    967757 |         2 | 30 |      14 |      77.71 |      1 | {77.71}                   | {30,14}
    967722 |         2 | 14 |       6 |      99.99 |      2 | {77.71,99.99}             | {30,14,6}
    967699 |         2 |  6 |       2 |      99.99 |      3 | {77.71,99.99,99.99}       | {30,14,6,2}
    967686 |         2 |  2 |       0 |      88.45 |      4 | {77.71,99.99,99.99,88.45} | {30,14,6,2,0}
(4 rows)
```
