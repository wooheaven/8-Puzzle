import json
from collections import defaultdict
import sys
import numpy as np
import gzip

class Policy_Iteration:
    def __init__(self, policy_file, reward_file):
        self.policy_file = policy_file
        self.reward_file = reward_file
        self.discount_factor = 0.9

    def save_random_policy_from_adjacency(self, read_file_name, save_file_name):
        random_policy = defaultdict(lambda: defaultdict(float))
        with open(file=read_file_name) as read_file:
            adjacency_dict = json.load(fp=read_file)
        for i_key in adjacency_dict.keys():
            for j_key in adjacency_dict[i_key].keys():
                random_policy[i_key][j_key] = 0.0
                random_policy[j_key][i_key] = 0.0
        for i_key in random_policy.keys():
            n = len(random_policy[i_key].keys())
            for j_key in random_policy[i_key].keys():
                random_policy[i_key][j_key] = round(1 / n * 100, 2)
        with open(file=save_file_name, mode='w') as save_file:
            json.dump(obj=random_policy, fp=save_file, indent=4)

    def policy_iteration(self, iteration_size, mode):
        for iteration in range(iteration_size):
            if iteration is 0:
                print('policy_creation\t\t: iteration 0 from files [' + str(self.policy_file) + ', ' + str(self.reward_file) + '] is started')
                with open(file=self.policy_file) as read_file:
                    self.policies = defaultdict(lambda : defaultdict(lambda: defaultdict(float)))
                    self.policies[str(iteration)] = json.load(fp=read_file)
                with open(file=self.reward_file) as read_file:
                    self.value_functions = defaultdict(lambda : defaultdict(float))
                    self.value_functions[str(iteration)] = json.load(fp=read_file)
                print('policy_creation\t\t: iteration 0 from files [' + str(self.policy_file) + ', ' + str(self.reward_file) + '] is ended')
            else:
                self.policy_evaluation(iteration)
                self.policy_improvement(iteration, mode)

    def policy_evaluation(self, iteration):
        # policy_evaluation = add next_iteration of value_functions_dict
        old_iteration = str(iteration - 1)
        new_iteration = str(iteration)
        print('policy_evaluation\t: iteration ' + str(new_iteration) + ' from iteration ' + str(old_iteration) + ' is started.')
        for id in self.value_functions[old_iteration].keys():
            value = 0.0
            value = self.value_functions[old_iteration][id] # This is new policy evaluation
            for next_id in self.policies[old_iteration][id].keys():
                reward = self.value_functions['0'][next_id]
                next_value = self.value_functions[old_iteration][next_id]
                proability = self.policies[old_iteration][id][next_id] / 100.0
                value += proability * (reward + self.discount_factor * next_value)
            self.value_functions[new_iteration][id] = value # This is Bellman Expectation Equation
        print('policy_evaluation\t: iteration ' + str(new_iteration) + ' from iteration ' + str(old_iteration) + ' is ended.')

    def policy_improvement(self, iteration, mode='s'):
        # policy_improvement = Greedy Policy
        if mode == 'g':
            old_iteration = str(iteration - 1)
            new_iteration = str(iteration)
            print('policy_improvement as greedy policy\t: iteration ' + str(new_iteration) + ' from iteration ' + str(old_iteration) + ' is started.')
            for from_id in self.policies[old_iteration].keys():
                max_value = 0
                max_index = []
                for to_id in self.policies[old_iteration][from_id].keys():
                    current_value = self.value_functions[new_iteration][to_id]
                    if max_value < current_value:
                        max_value = current_value
                        max_index.clear()
                        max_index.append(to_id)
                    elif max_value == current_value:
                        max_index.append(to_id)
                max_probability = 1.0 / len(max_index) * 100.0
                for to_id in self.policies[old_iteration][from_id].keys():
                    if to_id in max_index:
                        self.policies[new_iteration][from_id][to_id] = max_probability
                    else:
                        self.policies[new_iteration][from_id][to_id] = 0
            print('policy_improvement as greedy policy\t: iteration ' + str(new_iteration) + ' from iteration ' + str(old_iteration) + ' is ended.')
        # policy_improvement = Softmax Policy
        elif mode == 's':
            old_iteration = str(iteration - 1)
            new_iteration = str(iteration)
            print('policy_improvement as softmax policy\t: iteration ' + str(new_iteration) + ' from iteration ' + str(old_iteration) + ' is started.')
            for from_id in self.policies[old_iteration].keys():
                new_value_functions = []
                for to_id in self.policies[old_iteration][from_id].keys():
                    new_value_functions.append(self.value_functions[new_iteration][to_id])
                new_policy = self.softmax(new_value_functions)
                for index, to_id in enumerate(self.policies[old_iteration][from_id].keys()):
                    self.policies[new_iteration][from_id][to_id] = round(new_policy[index] * 100.0, 2)
            print('policy_improvement as softmax policy\t: iteration ' + str(new_iteration) + ' from iteration ' + str(old_iteration) + ' is ended.')
        # policy_improvement = As is Policy
        elif mode == 'a':
            old_iteration = str(iteration - 1)
            new_iteration = str(iteration)
            print('policy_improvement as asis policy\t: iteration ' + str(new_iteration) + ' from iteration ' + str(old_iteration) + ' is started.')
            for from_id in self.policies[old_iteration].keys():
                new_value_functions = []
                for to_id in self.policies[old_iteration][from_id].keys():
                    new_value_functions.append(self.value_functions[new_iteration][to_id])
                new_policy = self.percent(new_value_functions)
                for index, to_id in enumerate(self.policies[old_iteration][from_id].keys()):
                    self.policies[new_iteration][from_id][to_id] = round(new_policy[index], 2)
            print('policy_improvement as as'
                  'is policy\t: iteration ' + str(new_iteration) + ' from iteration ' + str(old_iteration) + ' is ended.')
        else:
            raise NameError("Policy Improvement is support only Grredy(g)/Softmax(s)/AsIs Policy(a). Your Policy Improvement mode is " + str(mode))

    def softmax(self, array):
        c = np.max(array)
        exp_a = np.exp(array - c)
        sum_exp_a = np.sum(exp_a)
        y = exp_a / sum_exp_a
        return y

    def percent(self, num_list):
        a = np.array(num_list)
        sum = np.sum(a)
        y = a / sum * 100.0
        return y

    def print_value_functions(self, sample_iterations, sample_ids):
        print('size of value_functions(byte) = ', sys.getsizeof(self.value_functions))
        print('iteration,id,value')
        for sample_iteration in sample_iterations:
            for sample_id in sample_ids:
                print(sample_iteration, ',', sample_id, ',', self.value_functions[str(sample_iteration)][str(sample_id)], sep='')

    def print_policies(self, sample_iterations, sample_ids):
        print('size of policies(byte) = ', sys.getsizeof(self.policies))
        print('iteration,from_id,to_id,proability')
        for sample_iteration in sample_iterations:
            for sample_id in sample_ids:
                for to_id in self.policies[str(sample_iteration)][str(sample_id)].keys():
                    print(sample_iteration, ',', sample_id, ',', to_id, ',', self.policies[str(sample_iteration)][str(sample_id)][to_id], sep='')

    def save_policies(self, policies_file_name, mode='gz'):
        print('save policy\t: ' + str(policies_file_name) + ' is started.')
        print('size of policies(byte) = ', sys.getsizeof(self.policies))
        if mode == 'gz':
            policies_str = json.dumps(obj=self.policies, indent=4)
            policies_bytes = policies_str.encode(encoding='utf-8')
            with gzip.GzipFile(filename=policies_file_name, mode='w') as save_file:
                save_file.write(policies_bytes)
        elif mode == 'json':
            with open(file=policies_file_name, mode='w') as save_file:
                json.dump(obj=self.policies, fp=save_file, indent=4)
        elif mode == 'csv':
            with open(file=policies_file_name, mode='w') as save_file:
                # save_file.writelines(["iteration,", "from_id,", "to_id,", "proability"])
                # save_file.write('\n')
                for iteration in self.policies.keys():
                    for from_id in self.policies[iteration].keys():
                        for to_id in self.policies[iteration][from_id].keys():
                            save_file.writelines(','.join([iteration, from_id, to_id, str(self.policies[iteration][from_id][to_id])]))
                            save_file.write('\n')
        print('save policy\t: ' + str(policies_file_name) + ' is ended.')

    def load_policies(self, policies_file_name, mode='gz'):
        if mode == 'gz':
            with gzip.GzipFile(filename=policies_file_name, mode='r') as read_file:
                policies_bytes = read_file.read()
            policies_json = policies_bytes.decode(encoding='utf-8')
            self.policies = json.loads(policies_json)
        elif mode == 'json':
            with open(file=policies_file_name, mode='r') as read_file:
                self.policies = json.load(fp=read_file)
        print('size of policies(byte) = ', sys.getsizeof(self.policies))
