# (state) ValueFunction 의 정의
Time t에서 State s에 대한 DisCountFactor를 반영한 미래 Reward의 총합을 ValueFunction이라고 한다.  
Time t에서 State s에 대한 Return을 ValueFunction이라고 한다.  
action ValueFunction = QFunction과 구분하기 위해서 state ValueFunction으로 사용하기도 한다.  
$` ValueFunction(s) = E[G_t | S_t = s] `$   
$` \kern6em\,\: v(s) = E[G_t | S_t = s] `$  

# ValueFunction을 Recursive하게 정리하기
$` v(S_t) = E[G_t \enspace | \enspace S_t = s] `$  
$` \kern2em\: = E[ \displaystyle\sum_{i=1}^\infty \gamma^{i-1} \cdot R_{t+i} \enspace | \enspace S_t =s ] `$  
$` \kern2em\: = E[ R_{t+1} + \displaystyle\sum_{i=2}^\infty \gamma^{i-1} \cdot R_{t+i} \enspace | \enspace S_t =s ] `$  
$` \kern2em\: = E[ R_{t+1} + \gamma \cdot v(S_{t+1}) \enspace | \enspace S_t = s ] `$  

# ValueFunction에 대한 Bellman Expectation Equation 유도하기
어떤 Agent가 Policy $` \pi `$를 따른다고 할때   
위의 Recursive하게 정리된 ValueFunction이 Bellman Expectation Equation이다.  

$` v_{\pi}(S_t) = E_{\pi} [ R_{t+1} + \gamma \cdot v_{\pi}(S_{t+1}) \enspace | \enspace S_t = s ]  `$  

기댓값 $` E_{\pi}`$ 을 계산 가능한 합의 형태로 바꾸면 아래와 같다.  

$` v_{\pi}(S_t) = \displaystyle\sum_{a \in A} {\pi}(a|S_t) \cdot \{ R_{t+1} + \gamma \cdot v_{\pi}(S_{t+1}) \}`$  

# BEE에 iteration 도입하기

위의 Bellman Expectation Equation을 이용하면 ValueFunction을 근사적으로 계산하기 위해 iteration을 도입하자.  
그러면 계산 가능한 벨만 기대 방정식을 아래처럼 얻을 수 있다.  

$` v_{\pi, k+1}(S_t) = \displaystyle\sum_{a \in A} {\pi}(a|S_t) \cdot \{ R_{t+1} + \gamma \cdot v_{\pi, k}(S_{t+1}) \}`$  

어떤 Agent가 Policy $`{\pi}`$ 를 따르고, Iteration k, Time t, State S 이라 하자.  
Iteration k+1, Time t, State S의 ValueFunction 을 Iteration k, Time t, State S의 관련 정보로 업데이트 한다는 의미이다.  
쉽게 말해, 어떤 상태 S의 ValueFunction을 이전 Iteration 의 정보를 이용하여  
재귀적으로 근사하여 그 값을 구한다는 뜻이다.  
