import dijkstra_label
import psycopg2
import numpy as np


# def get_next_ids(conn, id):
#     try:
#         cursor = conn.cursor()
#         query = "SELECT next_ids FROM states WHERE id = %s"
#         cursor.execute(query, (id,))
#         tuple = cursor.fetchone()
#         return list(map(int, tuple[0].split(',')))
#     except (Exception, psycopg2.DatabaseError) as error:
#         print("Error Happen")
#         print(query)
#         print(error)
#     finally:
#         if cursor.closed is False:
#             cursor.close()

if __name__ == '__main__':
    # Upload Nodes
    node_count = 181440
    log_path = "30node_dijkstra_label.log"
    g = dijkstra_label.Graph(node_count=node_count, log_path=log_path)

    # Upload Edges
    # conn = psycopg2.connect("dbname='mydatabase' user='myuser' host='localhost' port='65432' password='123qwe'")
    # for i in range(node_count):
    #     id = i + 1
    #     next_ids_list = get_next_ids(conn, id)
    #     for next_id in next_ids_list:
    #         if id < next_id:
    #             g.add_edge(i, next_id - 1, 1.0)
    g.read_adjacency_dict('adjacency_dict.json')

    # Save adjacency_dict
    # g.save_adjacency_dict('adjacency_dict.json')

    # Dijkstra_dict
    n = 14
    g.dijkstra(0, n)
    shortest_path, path_length = g.find_shortest_path(n)
    print(str(n) + '\t-> 0 = ', shortest_path, path_length)

    # Assert
    np.testing.assert_array_equal(shortest_path, [0, 2, 6, 14])
    np.testing.assert_equal(path_length, 3.0)

    # End
    # conn.close()