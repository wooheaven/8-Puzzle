import numpy as np
import logging
import json


class Graph():
    def __init__(self, node_count, log_path):
        self.node_count = node_count
        self.nodes = np.array(range(node_count))
        self.adjacency_dict = {}

        logging.basicConfig(level=logging.DEBUG,
                            format='%(asctime)s - %(name)s - %(levelname)s\t- %(message)s',
                            filename=log_path,
                            filemode='w')
        self.logger = logging.getLogger('dijkstra_label')
        self.logger.debug('Nodes = %s', self.nodes)
        self.logger.debug('Adjacency Dict = %s', self.adjacency_dict)

    def add_edge(self, node_a, node_b, distance):
        node_a_str = str(node_a)
        node_b_str = str(node_b)
        if node_a_str not in self.adjacency_dict.keys():
            self.adjacency_dict[node_a_str] = {}
        self.adjacency_dict[node_a_str][node_b_str] = distance
        self.logger.debug('add edge with a pair of nodes %s,%s and distance %s', node_a, node_b, distance)

    def read_adjacency_dict(self, file):
        with open(file=file) as read_file:
            self.adjacency_dict = json.load(fp=read_file)

    def save_adjacency_dict(self, file):
        with open(file=file, mode='w') as save_file:
            json.dump(obj=self.adjacency_dict, fp=save_file)

    def is_exsist_weight(self, u, v):
        u, v = self.sort_u_v(u, v)
        result = False
        if str(u) in self.adjacency_dict:
            if str(v) in self.adjacency_dict[str(u)]:
                result = True
        return result

    def sort_u_v(self, u, v):
        if v < u:
            u, v = v, u
        return u, v

    def get_weight(self, u, v):
        u, v = self.sort_u_v(u, v)
        return self.adjacency_dict[str(u)][str(v)]

    def dijkstra(self, start_node, end_node):
        self.logger.debug('dijkstra is started')
        self.logger.debug('Adjacency Dict = %s', self.adjacency_dict)
        self.start_node = start_node
        shortest_path_single_to_multi = [None for _ in range(self.node_count)]

        # Step 1
        S = np.array([start_node])
        Q = np.delete(self.nodes, start_node)
        label = np.array([None for _ in range(self.node_count)])
        label[start_node] = 0
        shortest_path_single_to_multi[start_node] = start_node
        for v in Q:
            label[v] = np.inf
        self.logger.debug('Step 1 : start_node = %s', start_node)
        self.logger.debug('Step 1 : end_node = %s', end_node)
        self.logger.debug('Step 1 : shortest_path_single_to_multi = %s', shortest_path_single_to_multi)
        self.logger.debug('Step 1 : label = %s', label)
        self.logger.debug('Step 1 : S = %s', S)
        self.logger.debug('Step 1 : Q = %s', Q)

        # Step 2
        LOG_MSG = 'Step 2,3 : u = %s, v = %s, label = %s, min_dis = %s, min_v = %s'
        while Q.size > 0 and len(np.argwhere(S == end_node)) == 0:
            min_dis = np.inf
            min_u = -1
            min_v = -1
            for v in Q:
                for u in S:
                    if self.is_exsist_weight(u, v):
                        self.logger.debug(LOG_MSG + '\tWeight(u,v) < inf', u, v, label, min_dis, min_v)
                        uv_path_length = label[u] + self.get_weight(u, v)
                        if uv_path_length < label[v]:
                            label[v] = uv_path_length
                            shortest_path_single_to_multi[v] = u
                            self.logger.debug(LOG_MSG + '\tLabel is updated', u, v, label, min_dis, min_v)
                        if label[v] < min_dis or (label[v] == min_dis and u < min_u):
                            min_dis = label[v]
                            min_v = v
                            min_u = u
                            self.logger.debug(LOG_MSG + '\tmin_u, min_v, min_dis are updated', u, v, label, min_dis, min_v)
            S = np.append(S, [min_v])
            Q = np.delete(Q, np.argwhere(Q == min_v))
            self.logger.debug('Step 1 : S = %s', S)
            self.logger.debug('Step 1 : Q = %s', Q)

        self.S = S
        self.Q = Q
        self.label = label
        self.shortest_path_single_to_multi = shortest_path_single_to_multi
        self.logger.debug('dijkstra is finished')

    def find_shortest_path(self, end_node):
        self.logger.debug('find_shortest_path is started')
        shortest_path_single_to_single = np.array([end_node])
        back_tracking_node = self.shortest_path_single_to_multi[end_node]
        shortest_path_single_to_single = np.append(shortest_path_single_to_single, back_tracking_node)
        while back_tracking_node != self.start_node:
            back_tracking_node = self.shortest_path_single_to_multi[back_tracking_node]
            shortest_path_single_to_single = np.append(shortest_path_single_to_single, back_tracking_node)
        shortest_path_single_to_single = shortest_path_single_to_single[::-1]
        length = self.label[end_node]
        self.logger.debug('%s -> %s shortest path = %s and length = %s',
                          self.start_node, end_node, shortest_path_single_to_single, length)
        self.logger.debug('find_shortest_path is finished')
        return shortest_path_single_to_single, length
