import dijkstra_label
import numpy as np

if __name__ == '__main__':
    # Upload Nodes
    node_count = 6
    log_path = "exam_dijkstra_label.log"
    g = dijkstra_label.Graph(node_count=node_count, log_path=log_path)

    # Upload Edges
    g.add_edge(0, 1, 3)
    g.add_edge(0, 2, 2)
    g.add_edge(0, 3, 4)
    g.add_edge(1, 3, 2)
    g.add_edge(1, 5, 5)
    g.add_edge(2, 4, 1)
    g.add_edge(3, 4, 1)
    g.add_edge(3, 5, 3)
    g.add_edge(4, 5, 2)
    # g.read_adjacency_dict('exam_adjacency_dict.json')

    # Save adjacency_dict
    # g.save_adjacency_dict('exam_adjacency_dict.json')

    # Dijkstra_dict
    g.dijkstra(0, 5)
    shortest_path, path_length = g.find_shortest_path(5)

    # Assert
    np.testing.assert_array_equal(shortest_path, [0, 2, 4, 5])
    np.testing.assert_equal(path_length, 5.0)
