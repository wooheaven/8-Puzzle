from gephistreamer import graph
from gephistreamer import streamer
import json


# ref : https://github.com/totetmatt/GephiStreamer
# Create a Streamer
# adapt if needed : streamer.GephiWS(hostname="localhost", port=8080, workspace="workspace0")
# You can also use REST call with GephiREST (a little bit slower than Websocket)
stream = streamer.Streamer(streamer.GephiWS(hostname='192.168.3.47', workspace="workspace1"))

file = 'adjacency_dict.json'
adjacency_dict = None
with open(file=file) as read_file:
    adjacency_dict = json.load(fp=read_file)

node_num = -1
for i_key in adjacency_dict.keys():
    i = int(i_key)
    if i > 29:
        break
    else:
        for j_key in adjacency_dict[i_key].keys():
            weight = adjacency_dict[i_key][j_key]
            if weight > 0:
                # i = int(i_key)
                j = int(j_key)
                node_a = graph.Node(i)
                node_b = graph.Node(j)
                if node_num < i:
                    stream.add_node(node_a)
                    node_num = i
                if node_num < j:
                    stream.add_node(node_b)
                    node_num = j
                edge = graph.Edge(source=node_a, target=node_b, directed=False, weight=weight)
                stream.add_edge(edge)

print('test')