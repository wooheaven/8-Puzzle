from gephistreamer import graph
from gephistreamer import streamer


# ref : https://github.com/totetmatt/GephiStreamer
# Create a Streamer
# adapt if needed : streamer.GephiWS(hostname="localhost", port=8080, workspace="workspace0")
# You can also use REST call with GephiREST (a little bit slower than Websocket)
stream = streamer.Streamer(streamer.GephiWS(hostname='192.168.3.47', workspace="workspace1"))

for i in range(6):
    node = graph.Node(i)
    stream.add_node(node)

adjacency_matrix = [[0., 3., 2., 4., 0., 0.],
                    [3., 0., 0., 2., 0., 5.],
                    [2., 0., 0., 0., 1., 0.],
                    [4., 2., 0., 0., 1., 3.],
                    [0., 0., 1., 1., 0., 2.],
                    [0., 5., 0., 3., 2., 0.]]

for i in range(len(adjacency_matrix)):
    for j in range(i + 1, len(adjacency_matrix[i])):
        weight = adjacency_matrix[i][j]
        if weight > 0:
            node_a = graph.Node(i)
            node_b = graph.Node(j)
            edge = graph.Edge(source=node_a, target=node_b, directed=False, weight=weight)
            stream.add_edge(edge)